# Safety rules in Fab Lab Oulu

Watch Fab Lab Rules 2 min video made by Harri Hämeenkorpi, Dorina
Rajanen, and Mikko Rajanen:

https://vimeo.com/296006151/0ca1b7be4a

## Best practices:

* Think about and plan your task before starting using the equipment (For example, read the documentation of the task, and prepare the materials you need).
* If you are in doubt or need help, ask Fab Lab Oulu staff for guidance and information.
* Software tools have also own documentation and user guides; please feel free to consult these by yourself and ask guidance from Fab Lab Oulu staff whenever needed.
* Be community-minded (help others, clean your mess, document your work, place the tools you have used back to their place).
* Be nature-friendly (be aware of waste, sort the hazardous waste according to instructions, design and fabricate to minimize the waste and resources (energy consumption, wear of equipment, etc.)).

## General rules:

* While operating the equipment focus on the task at hand.
* Do not leave the equipment and machines running unattended.
* Stop the machine if you observe it does not run correctly and smoothly or if hazardous materials start to burn or smoke.
* Leave all the workspaces you have used CLEAN and NEAT when your task is completed.
* When finishing working with any machine (except computers, 3D printers, and laser cutter), shut it down (For example, electronics workbench station, soldering iron, reflow oven, milling machines must be shut down after use).
* Do not use equipment you have not been trained to use.
* Wash hands with soap after working in various processes in Fab Lab.
* It is not recommended to eat or drink in the lab.
* Report any safety issues to the Fab Lab staff.
* Hearing protection is available. Ask Fab Lab staff.

Read also about **Fab Lab health and safety in general**

[http://wiki.fablab.is/wiki/Portal:Safety](http://wiki.fablab.is/wiki/Portal:Safety)

[http://wiki.fablab.is/wiki/Portal:Safety](Safety_rules_in_Fab_Lab_Oulu)

## Electronics production

During the Electronics Production Process the following safety rules
should be applied:

Using the Electronics Workbench for manual soldering:

* Before start soldering, TURN ON THE VENTILATION. **First**, ensure that the two switches next to the door are turned on (they have to be turned to the right when they are ON). **Second**, each electronics workbench has a tube for ventilation that has to be switched on. Ask for help from Fab Lab staff if you are unsure of how the ventilation works.
* Always wash hands with soap after soldering (whether you have soldered with the soldering iron or with pick-and-place system and reflow).
* Shut down the electronic station after use.
* Do NOT eat and drink in the electronics workshop.

Using the Reflow Oven for soldering:

* Make sure you have a suitable PCB material for the reflow oven. Only FR4 should be used with the oven. If you have FR1 you should use manual soldering. Ask Fab Lab staff if you are unsure of the material of your PCB.
* Use gloves to place/remove your PCB when the oven is heated.
* Do not touch any elements inside machine.
* Allow PCB to cool after reflowing.
* Only use the machine interface controls to open/close the drawer.
* Shut down the machine after use.
* Wash your hands with soap after soldering.

Using the Milling machine:

* Changing the milling bits: + Pause or turn off the machine before changing the bits. + Select the rights bits; the bits should be kept in their holders until they are required. + Handle the bits with care not to break them or to hurt yourself.
* Keep work area clear of waste material and offcuts; Vacuum the milling residuals before changing the bits and at the end of the job.
* Shut down the machine after use.
* Remove the milling bit from the machine after use and put it in its holder.
* Wash your hands with soap after using the milling machine.

## Laser cutting

During the Laser Cutting and Engraving Process the following safety
rules should be applied:

* First, ensure that the air input and output are open (see instruction on the wall near the machine).
* Never cut unknown materials.
* Always consult the list of allowed /not allowed materials.
* Never leave the machine unattended.
* At the end of the job, wait one minute before you open the lid. In this way you allow the toxic gases to be removed from the machine and not enter the room.

## 3D printing

* Ensure you have the correct materials loaded into the machine.
* Use the correct protective gloves to handle different parts of the print and machine; some surfaces in the machines are very hot; some materials are harmful for skin; some parts are sharp. Read the safety issues of the 3D printing machine that you are using (see the links below).
* Read and follow the safety issues for [Form 2](Safety_issues_with_the_Form_2_printer).
* Read and follow the safety issues for [Stratasys](Safety_issues_with_the_Stratasys_3D_printer).
* Read and follow the safety issues for Leapfrog.
* Materials typically used in Fab Lab Oulu with the 3D printers (ABS, resin, PLA) are NOT food safe.
