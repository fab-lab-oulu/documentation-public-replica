# Printing with Formlabs Form 2

See [information about Form 2 printer](Formlabs).

For printing a 3D model, you need first to create the model with a CAD
tool for 3D modeling and save it as .stl. See [design specs for Form
2](https://formlabs.com/3d-printers/design-specs/) and [guidlines on 3D
modeling](Computer-aided_3D_design).

Then you need to set up the printer (see [a tutorial on how to set up
Form 2](https://support.formlabs.com/hc/en-us/articles/115000013424-Set-Up-Your-Form-2),
also reproduced below).

Last step is printing (see [a tutorial on how to print a 3D model with
Form 2](https://support.formlabs.com/hc/en-us/articles/115000013824),
also reproduced below).

See [safety issues of Form 2](Safety_issues_with_the_Form_2_printer)
(also reproduced below).

## Setting up the printer (based on [the tutorial on how to set up Form 2](https://support.formlabs.com/hc/en-us/articles/115000013424-Set-Up-Your-Form-2))

### 1. Insert the Resin Tank with the Wiper

Lift the printer’s cover. Remove the resin tank’s black lid and align
the four small feet of the resin tank into the corresponding holes in
the tank carrier.

![](attachments/69798117/69894594.png)
![](attachments/69798117/69894595.png)

Push the tank until it is flush against the front of the carrier. The
printer will not properly detect the tank unless the tank is fully
inserted.

### 1. Lock the Wiper

Ensure the wiper is straight, then align the foot of the wiper with the
wiper mount.

![](attachments/69798117/69894596.png)
![](attachments/69798117/69894597.png)

Push the wiper toward the tank to align it with the front of the mount.
The wiper should be firmly mounted.

#### Tip: Manually slide wiper

The wiper mount will home to the far right at the end of a print.
Manually slide the wiper mount to the center before removing a resin
tank.

### 1. Insert the Build Platform

Align the build platform with the platform carrier and push into place.
Lock the handle down to secure the build platform.

![](attachments/69798117/69894598.png)
![](attachments/69798117/69894599.png)

### 1. Shake the Resin Cartridge

Before inserting a new cartridge, shake the cartridge to ensure the
resin is well-mixed.

![](attachments/69798117/69894600.png)

#### Tip: Shake resin regulary

Shake the resin cartridge approximately every two weeks to keep the
formula well-mixed for the best print quality.

Then, remove the protective valve cover from the underside of the
cartridge. Consider saving the cover to protect the valve during
storage.

#### WARNING 1

Do not remove the rubber valve at the bottom of the cartridge. This bite
valve controls the release of the resin. Removing the rubber bite valve
would allow resin to continuously flow and cause extreme damage to the
machine.

<img src="https://support.formlabs.com/hc/en-us/article_attachments/115000038924/_M9A7787_fnl.jpg.1354x0_q80_crop-smart.jpg" width="600" />

### 1. Insert the Resin Cartridge

Align the cartridge with the opening at the back of the printer. Push
down on the cartridge handle until the top of the cartridge is level
with the printer.

![](attachments/69798117/69894601.png)

Be sure to press open the vent cap before starting a print, so that your
resin tank fills correctly.

<img src="https://support.formlabs.com/hc/en-us/article_attachments/115000038944/_M9A7626_fnl1.jpg.1354x0_q80_crop-smart.jpg" width="600" />

#### TIP: Check material

Make sure the material in your resin tank always matches the resin type
in the installed cartridge.

### 1. Power the Printer On

Now you are ready to print. See the steps of printing below.

## Main steps of printing (based on [the tutorial on how to print a 3D model with Form 2](https://support.formlabs.com/hc/en-us/articles/115000013824))

1. Open the **.stl** file in
    [PreForm](https://formlabs.com/tools/preform/) software. There you can scale, orient, and create or modify the supports for each model, then save the print as a **.form** before uploading.
1. Confirm the type of resin and layer height before sending a
    **.form** file to the Form 2.
1. To start the upload process, select the orange printer icon in the PreForm toolbar.
1. To

<img src="https://support.formlabs.com/hc/en-us/article_attachments/115000039464/pre-upload.png.1354x0_q80_crop-smart.png" width="900" />

If the orange icon is not available, the printer may not be properly
connected to the same local network. Check to see that you do not have
other open instances of PreForm and learn more about [connecting to your
Form 2.](https://support.formlabs.com/hc/articles/115000011130) Save any
files before restarting PreForm, and re-connect your printer to
establish a proper connection

### Confirming

From the print queue view, the Form 2’s touchscreen will display the
**.form** file’s upload in progress. Use the screen to select the file
name and start the print.

Follow the onscreen prompts. The Form 2 will automatically fill and warm
the tank, then the print will start automatically.

### Managing Uploaded Prints

The Form 2 can save **.form** files to easily restart recently stored
print jobs from the queue.

#### Start a Previous Print

To start a saved file, confirm your resin tank and cartridge match the
file’s settings, then simply select the file’s name from the queue to
begin the print.

#### Delete a Print Job

To delete a print from the queue, select the file name from the list and
then select the delete icon on the touchscreen's bottom left corner.

## Safety issues (based on [vendor safety information](https://support.formlabs.com/hc/en-us/articles/115000011604-Safety))

The Form 2 is a precision tool that requires respect and care to ensure
safe operation. Follow all safety considerations during operation.

Like any professional equipment, you should treat the printer,
materials, and accessories with respect and care to ensure a safe
working environment and a long-lasting machine.

### Supervision

The Form 2 is an excellent educational tool.

The tool should be used with proper training and the supervision of
young, inexperienced users to ensure enjoyable and safe operation.

### Do Not Modify

The Form 2 printer is for use as-is. The printer should not be modified
without explicit approval and directions from Formlabs, otherwise
warranty will be invalid, the machine can be ruined, and can cause
bodily harm to users.

### Laser

![](https://support.formlabs.com/hc/en-us/article_attachments/115000025450/lasercaution.png)Caution
when working with lasers.

The Form 2 is a Class 1 Laser product. Accessible radiation is within
Class 1 limits. The laser diode used inside the device has the following
specifications:

**Diode**: Violet (405nm)

**Max Output**: 250 mW

Never remove the front or back panels of the printer unless instructed
to do so by Formlabs Support. This will expose you to danger and void
your
[warranty.](https://formlabs.com/support/terms-of-service/#warranty) The
laser beam is harmful to the eyes, so avoid direct contact. The Form
2 contains an interlock system to automatically shut off the laser when
the orange cover is opened. If this system is tampered with, or fails,
there is risk of exposure to Class 3B laser light.

#### Laser Certification

– IEC 60825-1:2007
– EN 60825-1:2007
FDA performance standards for laser products except for deviations
pursuant to Laser Notice No. 50, dated June 24, 2007.

#### WARNING 2

**Class 1 Laser Product. Never remove the front or back panels of the printer.**

### Resin

Respect Formlabs resin like any household chemical. Follow standard
chemical safety procedures and [Formlabs resin handling
instructions](https://support.formlabs.com/hc/articles/115000018044).
Wear gloves whenever handling liquid resin.

In general, Formlabs resin is not approved for use with food, drink, or
medical applications on the human body. However, biocompatible resins,
such as Dental SG, are biologically safe for specific types and lengths
of exposure to the human body. Refer to information about each specific
resin for more detail. Never ingest resin in liquid or solid form.

#### Tip: Read safety instructions

Always consult the [SDS (Safety Data Sheet)](http://formlabs.com/products/3d-printers/tech-specs/#material-properties)
as the primary source of information to understand safety and handling
of Formlabs materials.

### Isopropyl Alcohol (IPA)

![](https://support.formlabs.com/hc/en-us/article_attachments/115000024444/flammablehazardsymbol.png)Isopropyl
alcohol is a flammable chemical.

Carefully follow the safety instructions provided with the isopropyl
alcohol that you purchase. Isopropyl alcohol can be flammable, even
explosive, and should be kept away from heat, fire, or sparks. Any
containers holding isopropyl alcohol should be kept closed or covered
when not in use. We also recommend that you wear protective gloves and
have good ventilation when working with IPA.

#### WARNING 3

Formlabs does not manufacture isopropyl alcohol. You should consult the
chemical manufacturer or supplier for more in depth safety information.
[Learn how to handle IPA with your Form 2](https://support.formlabs.com/hc/articles/115000024624).

### Sharp Tools

The accessories kit includes sharp tools such as: tweezers, ﬂush
cutters, a scraper, and a part removal tool. Using these tools on
slippery surfaces (such as a resin-coated build platform) can result in
sudden movement.

### Radio Interference

This equipment has been tested and found to comply with the limits for a
Class B digital device, pursuant to CFR Title 47, Part 15 of FCC Rules.
These limits are designed to provide reasonable protection against
harmful interference when the equipment is operated in a commercial
environment. This equipment generates, uses, and can radiate radio
frequency energy and, if not installed and used in accordance with the
instruction manual, may cause harmful interference to radio
communications. Operation of this equipment in a residential area is
likely to cause harmful interference in which case the user will be
required to correct the interference at their own expense.
