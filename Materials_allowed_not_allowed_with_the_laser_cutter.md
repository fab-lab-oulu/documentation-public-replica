# Materials allowed/not allowed with the laser cutter

## There are a wide range of materials that our Epilog laser can cut or etch - but some simply don't work (e.g., metals) and some are extremely hazardous to either humans or the machine itself (e.g., PVC and Vinyl)

It is therefore imperative that you check these lists before attempting
to cut materials that you have not worked with before.

It is not always obvious which materials will work - for example:
Polycarbonate/Lexan produces flames and lethal chlorine gas which will
rapidly corrode the machine into uselessness **and** which is extremely
hazardous to the health of people nearby. So check and double-check what
you're cutting.

Check also the safety rules of using the laser cutter and do not use the
laser cutter without being trained by Fab lab Oulu staff.

*Note: The guidelines in this page are from
[http://www.atxhackerspace.org/wiki/Laser_Cutter_Materials#NEVER_CUT_THESE_MATERIALS](http://www.atxhackerspace.org/wiki/Laser_Cutter_Materials#NEVER_CUT_THESE_MATERIALS).
If you are interested in testing some of the allowed materials as to
their properties when laser cutting and engraving, and then document
this work in a research project contact us at Fab Lab Oulu.*

## **Never cut these materials:**

|                                                             |                                           |                                                                                                                                                                                                                                                                                                                                                         |
|-------------------------------------------------------------|-------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  Not allowed material                                       | DANGER!                                   | Cause/Consequence                                                                                                                                                                                                                                                                                                                                       |
| PVC (Poly Vinyl Chloride)/vinyl/pleather/artificial leather | Emits chlorine gas when cut!              | Don't ever cut this material as it will ruin the optics, cause the metal of the machine to corrode, and ruin the motion control system.                                                                                                                                                                                                                 |
| Thick ( &gt;1mm ) Polycarbonate/Lexan                       | Cuts very poorly, discolors, catches fire | Polycarbonate is often found as flat, sheet material. The window of the laser cutter is made of Polycarbonate because *polycarbonate strongly absorbs infrared radiation!* This is the frequency of light the laser cutter uses to cut materials, so it is very ineffective at cutting polycarbonate. Polycarbonate is a poor choice for laser cutting. |
| ABS                                                         | Melts                                     | ABS does not cut well in a laser cutter. It tends to melt rather than vaporize, and has a higher chance of catching on fire and leaving behind melted gooey deposits on the vector cutting grid. It also does not engrave well (again, tends to melt).                                                                                                  |
| HDPE/milk bottle plastic                                    | Catches fire and melts                    | It melts. It gets gooey. Don't use it.                                                                                                                                                                                                                                                                                                                  |
| PolyStyrene Foam                                            | Catches fire                              | It catches fire, it melts, and only thin pieces cut. This is the \#1 material that causes laser fires!!!                                                                                                                                                                                                                                                |
| PolyPropylene Foam                                          | Catches fire                              | Like PolyStyrene, it melts, catches fire, and the melted drops continue to burn and turn into rock-hard drips and pebbles.                                                                                                                                                                                                                              |
| Fiberglass                                                  | Emits fumes                               | It's a mix of two materials that cant' be cut. Glass (etch, no cut) and epoxy resin (fumes)                                                                                                                                                                                                                                                             |
| Coated Carbon Fiber                                         | Emits noxious fumes                       | A mix of two materials. Thin carbon fiber mat can be cut, with some fraying - but not when coated.                                                                                                                                                                                                                                                      |

## **Safe Materials**

The laser can cut or engrave the materials in the table below. The
materials that the laser can cut are wood, paper, cork, and some kinds
of plastics. Etching can be done on almost anything, wood, cardboard,
aluminum, stainless steel, plastic, marble, stone, tile, and glass.

## **Safe to cut, but pay attention to warnings**

<table class="confluenceTable">
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Material</th>
<th class="confluenceTh">Max thickness</th>
<th class="confluenceTh">Notes</th>
<th class="confluenceTh">WARNINGS!</th>
</tr>

<tr class="odd">
<td class="confluenceTd">Many woods</td>
<td class="confluenceTd">1/4"</td>
<td class="confluenceTd">Avoid oily/resinous woods</td>
<td class="confluenceTd">Be very careful about cutting oily woods, or very resinous woods as they also may catch fire.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Plywood/Composite woods</td>
<td class="confluenceTd">1/4"</td>
<td class="confluenceTd">These contain glue, and may not laser cut as well as solid wood. Use plywood that is sold as laser-cuttable.</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="odd">
<td class="confluenceTd">MDF/Engineered woods</td>
<td class="confluenceTd">1/4"</td>
<td class="confluenceTd">These are okay to use but may experience a higher amount of charring when cut.</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="even">
<td class="confluenceTd">Paper, card stock</td>
<td class="confluenceTd">thin</td>
<td class="confluenceTd">Cuts very well on the laser cutter, and also very quickly.</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="odd">
<td class="confluenceTd">Cardboard, carton</td>
<td class="confluenceTd">thicker</td>
<td class="confluenceTd">Cuts well but may catch fire.</td>
<td class="confluenceTd">Watch for fire.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Cork</td>
<td class="confluenceTd">1/4"</td>
<td class="confluenceTd">Cuts nicely, but the quality of the cut depends on the thickness and quality of the cork. Engineered cork has a lot of glue in it, and may not cut as well.</td>
<td class="confluenceTd">Avoid thicker cork.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Acrylic/Lucite/Plexiglas/PMMA</td>
<td class="confluenceTd">1/2"</td>
<td class="confluenceTd">Cuts extremely well leaving a beautifully polished edge.</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="even">
<td class="confluenceTd">Thin Polycarbonate Sheeting (&lt;1mm)</td>
<td class="confluenceTd">&lt;1mm</td>
<td class="confluenceTd">Very thin polycarbonate can be cut, but tends to discolor badly. Extremely thin sheets (0.5mm and less) may cut with yellowed/discolored edges. Polycarbonate absorbs IR strongly, and is a poor material to use in the laser cutter.</td>
<td class="confluenceTd">Watch for smoking/burning</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delrin (POM)</td>
<td class="confluenceTd">thin</td>
<td class="confluenceTd">Delrin comes in a number of shore strengths (hardness) and the harder Delrin tends to work better. Great for gears!</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="even">
<td class="confluenceTd">Kapton tape (Polyimide)</td>
<td class="confluenceTd">1/16"</td>
<td class="confluenceTd">Works well, in thin sheets and strips like tape.</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="odd">
<td class="confluenceTd">Mylar</td>
<td class="confluenceTd">1/16"</td>
<td class="confluenceTd">Works well if it's thin. Thick mylar has a tendency to warp, bubble, and curl</td>
<td class="confluenceTd">Gold coated mylar will not work.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Solid Styrene</td>
<td class="confluenceTd">1/16"</td>
<td class="confluenceTd">Smokes a lot when cut, but can be cut.</td>
<td class="confluenceTd">Keep it thin.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Depron foam</td>
<td class="confluenceTd">1/4"</td>
<td class="confluenceTd">Used a lot for hobby, RC aircraft, architectural models, and toys. 1/4" cuts nicely, with a smooth edge.</td>
<td class="confluenceTd">Must be constantly monitored.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Gator foam</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Foam core gets burned and eaten away compared to the top and bottom hard paper shell.</td>
<td class="confluenceTd">Not a fantastic thing to cut, but it can be cut if watched.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Cloth/felt/hemp/cotton</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">They all cut well. Our "advanced" laser training class teaches lace-making.</td>
<td class="confluenceTd">Not plastic coated or impregnated cloth!</td>
</tr>
<tr class="even">
<td class="confluenceTd">Leather/Suede</td>
<td class="confluenceTd">1/8"</td>
<td class="confluenceTd">Leather is very hard to cut, but can be if it's thinner than a belt (call it 1/8"). Our "Advanced" laser training class covers this.</td>
<td class="confluenceTd">Real leather only! <strong>Not</strong> 'pleather' or other imitations!</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Magnetic Sheet</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Cuts beautifully</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="even">
<td class="confluenceTd">NON-CHLORINE-containing rubber</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Fine for cutting.</td>
<td class="confluenceTd">Beware chlorine-containing rubber!</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Teflon (PTFE)</td>
<td class="confluenceTd">thin</td>
<td class="confluenceTd">Cuts OK in thin sheets</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="even">
<td class="confluenceTd">Carbon fiber mats/weave<br />
that has <strong>not</strong> had epoxy applied</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Can be cut, very slowly.</td>
<td class="confluenceTd">You must not cut carbon fiber that has been coated!!</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Coroplast ('corrugated plastic')</td>
<td class="confluenceTd">1/4"</td>
<td class="confluenceTd">Difficult because of the vertical strips. Three passes at 80% power, 7% speed, and it will be slightly connected still at the bottom from the vertical strips.</td>
<td class="confluenceTd"> </td>
</tr>
</tbody>
</table>

## **Safe to etch, but pay attention to warnings**

All the above "cuttable" materials can be etched (engraved), in some
cases very deeply.

In addition, you can etch (engrave):

|                                          |                                               |                                                                                                                      |
|------------------------------------------|-----------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| Material                                 | Notes                                         | WARNINGS!                                                                                                            |
| Glass                                    | Green seems to work best...looks sandblasted. | Only flat glass can be etched typically, but Fab Lab Oulu has also a special support for round or cylindrical items. |
| Ceramic tile                             |                                               |                                                                                                                      |
| Anodized aluminum                        | Vaporizes the anodization away.               |                                                                                                                      |
| Painted/coated metals                    | Vaporizes the paint away.                     |                                                                                                                      |
| Stone, Marble, Granite, Soapstone, Onyx. | Gets a white "textured" look when etched.     | 100% power, 50% speed or less works well for etching.                                                                |

## Resources, links and references on this topic:

[http://www.atxhackerspace.org/wiki/Laser_Cutter_Materials#NEVER_CUT_THESE_MATERIALS](http://www.atxhackerspace.org/wiki/Laser_Cutter_Materials#NEVER_CUT_THESE_MATERIALS)

[https://www.epiloglaser.com/products/fusion-laser-series.htm](https://www.epiloglaser.com/products/fusion-laser-series.htm)

[http://arts.unl.edu/documents/art/DigitalLab/Epilog%20Approved%20Materials%20List.pdf](http://arts.unl.edu/documents/art/DigitalLab/Epilog%20Approved%20Materials%20List.pdf)

[http://fablabdevon.org/wp-content/uploads/2014/08/Laser_Safe_Materials1.pdf](http://fablabdevon.org/wp-content/uploads/2014/08/Laser_Safe_Materials1.pdf)
