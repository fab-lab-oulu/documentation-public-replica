# Processes

This page provides general tutorials on the following processes:

- Computer-aided 2D design
- Computer-aided 3D design
- [Laser cutting and engraving](Laser_cutting)
- [Vinyl cutting](Vinyl_cutting)
- [3D printing](3D_printing)
- [Electronic design and production](Electronics_design_and_production)
- [Embedded programming](Embedded_Programming)
- [CNC Machining](CNC+Machining)
- Digital Embroidery
