# Setting Up a Repository for Documentation in GitLab

For creating documentation in the different courses we are using a [GitLab](https://gitlab.com/) instance running in university own machines. The URL to access such service is [https://version.oulu.fi/users/sign_in](https://version.oulu.fi/users/sign_in).

In order to access to such service you must be inside University of Oulu network, so you must either use one the university computers using your own UNIV account, or access it using a VPN client. If you do not know how to set-up the VPN client University of Oulu IT services [provides some useful instructions](https://www.oulu.fi/en/for-students/supporting-your-studies-and-contact-information-students/it-services-students/remote-desktops).

**Steps you must follow:**

1. Login to the service, using the _University of Oulu login button_. Do not use the username and password. You must enter your university credentials ![image](uploads/c9fb4c06a8c0ca4c009efbac68fc4a6d/image.png)

1. Once inside you can create a project from scratch (if you know what you are doing). We have a default template that was in used in the past by Fab Academy students. It is based in [Mkdocs](https://www.mkdocs.org/) static site generator and utilize [Material for Mkdocs](https://squidfunk.github.io/mkdocs-material/) theme. **If you want to use this template you must fork the [our repository](https://version.oulu.fi/ivansanc/fabacademy_template_site)** ![image](uploads/c22617bbc633cc9504c71e2f22710094/image.png)

The next figure represents the appearance of the created website

![image](uploads/6e45887f98c1ab73eb0a1bac2a937b90/image.png).

1. When you fork the repository ... TBD
