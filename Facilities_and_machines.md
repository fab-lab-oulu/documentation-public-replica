# Facilities and Machines

## Facilities

Fab Lab Oulu is located in the campus of the University of Oulu, rooms TF 132-135, entrance 2T.
Finding your way inside the campus: walk along the "orange lane", Koneenkatu. Our premises are clearly marked as "Fab Lab Oulu", TF 135.

Street address: [Erkki Koiso-Kanttilan katu 3](https://maps.app.goo.gl/78V4CZdRxyYa5HWN6).
The bus stops [Yliopisto E](https://www.oulunliikenne.fi/-/Yliopisto%20E%3A%3A65.058572%2C25.469778/lahellasi?time=1716663098) and [Yliopisto P](https://www.oulunliikenne.fi/-/Yliopisto%20P%3A%3A65.059847%2C25.469936/lahellasi?time=1716663098) are very near to its entrance, as well as a taxi stop. See the [campus map](attachments/68813709/69206403.pdf)  for instructions on how to reach the Lab.

## Machines

Fab Lab Oulu has the following machines, among others:

| Machine | Description |
| --- | --- |
| [Laser cutter](Laser_cutter) | Makes 2D and 3D structures |
| [3D printers](3D_printers) | Various 3D printers of different sizes and performance: a professional scale Stratasys machine, a resin-based machine for creating small scale and precision 3D parts, a Leapfrog 3D printer for rapid prototyping and many Creality printers.|
| [Vinyl cutter](Vinyl_cutter) | Creates versatile 2D models on different materials such as vinyl, paper, copper sheets to make signs, indicators, T-shirt imprints, antennas, and flex circuits |
| [High-resolution CNC milling machine](Precision_CNC_milling) | Makes circuit boards and precision parts |
| [Large wood router](Wood_router) | For building furniture and housing |
| [Electronics workbench](Electronics_workbench) | A suite of electronic components and programming tools for low-cost, high-speed microcontrollers for on-site rapid circuit prototyping |
| [Meeting space](Computer_and_meeting_space_Parvi_) | Space for meetings, training, and presentations comprising of 6 computers for 2D and 3D design, a large screen for presentations, and a white board |

For tutorials on using the machines, check the page [Processes](Processes).
