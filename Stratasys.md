# Stratasys

Stratasys is a 3D printer for professional quality printing.

You can check the price of your 3D printing just by using this [excel
sheet](attachments/69797335/74613210.xlsx). Just fill the document with
the amount of material and support that you need as well as an
estimation of the build sheet used.

## Attachments:

[FabLab Calculator for Stratasys printing costs.xlsx](attachments/69797335/74613210.xlsx)
