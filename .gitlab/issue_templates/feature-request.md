<!---
Please read this!

Before opening a new feature proposal, make sure to search for keywords in the issues filtered by the "feature proposal" label and verify the issue you're about to submit isn't a duplicate.
--->

### Problem to solve

(What problem do we solve?)

### Target audience

(For whom are we doing this?)

### Further details

(Include use cases, benefits, and/or goals (contributes to our vision?))

### Proposal

(How are we going to solve the problem?)

/label ~"type: feature"
