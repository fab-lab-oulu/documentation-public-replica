# 3D printers

Fab Lab Oulu has different types of 3D printers:  

* [Stratasys](https://wiki.oulu.fi/display/FLOWS/Stratasys) is a 3D printer for professional quality printing. You can check the price of your 3D printing just by using this excel sheet. Just fill the document with the amount of material and support that you need as well as an estimation of the build sheet used.
* [Formlabs](https://wiki.oulu.fi/display/FLOWS/Formlabs) is a 3D printer for high precision small scale 3D models.
* [Leapfrog](https://wiki.oulu.fi/display/FLOWS/Leapfrog) is a 3D printer for rapid prototyping.
* In-house made 3D printer by Mikko Toivonen.

See [guidelines on 3D modeling](Computer-aided_3D_design) and guidelines on [3D printing](3D_printing).

<img src="https://www.oulu.fi/sites/default/files/fablab_3Dprinter_0.jpg" width="500" />

![](http://archive.fabacademy.org/archives/2017/fablaboulu/students/70/images/week5/printterit.jpg)

* Photo from http://archive.fabacademy.org/archives/2017/fablaboulu/students/70/week/_5.html
