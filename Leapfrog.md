# Leapfrog

## **Leapfrog 3D printer** is used for rapid prototyping

### Model: Leapfrog Creatr 3D Printer

![](attachments/69797339/70648003.png)

Vendor website:
[https://www.lpfrg.com/en/products/printer-compare/creatr-1/](https://www.lpfrg.com/en/products/printer-compare/creatr-1/)

**Technical specifications:**

| General                                         |                                             |
|-------------------------------------------------|---------------------------------------------|
| CE Certified                                    | Yes                                         |
| Dual Extruder                                   | No                                          |
| Built-in Webcam                                 | No                                          |
|                                                 |                                             |
| **Physical dimensions of the printer**          |                                             |
| Weight (kg)                                     | 32                                          |
| Weight (lbs)                                    | 70.5                                        |
| Body / Construction                             | Aluminium extrusion profiles CNC milled aluminium parts |
| Printer dimensions DxWxH (mm)                   | 600 x 500 x 500                             |
| Printer dimensions DxWxH (IN)                   | 23.6 x 19.6 x 19.6                          |
|                                                 |                                             |
| **Materials**                                   |                                             |
| Nozzle temperature (up to)                      | 275° C                                      |
| Nozzle temperature (up to)                      | 527° F                                      |
| Compatible filaments                            | ABS, PLA                                    |
|                                                 |                                             |
| **Precision**                                   |                                             |
| Layer height (mm)                               | 0.02 - 0.35                                 |
| Layer height (IN)                               | 0.00079 - 0.014                             |
| Positioning precision XY                        | 16.9 microns                                |
| Positioning precision Z                         | 20 microns                                  |
| Positioning precision XY                        | 0.67 mil                                    |
| Positioning precision Z                         | 0.78 mil                                    |
| Extruder / nozzle diameter (mm)                 | 0.35                                        |
| Extruder / nozzle diameter (IN)                 | 0.013                                       |
| Operation heated bed temperature                | 95° C                                       |
| Heated printing environment                     | No                                          |
|                                                 |                                             |
| **Speed**                                       |                                             |
| Printing speed (mm/s)                           | 60                                          |
| Printing speed (IN/s)                           | 2.36                                        |
| Max. flow rate (mm³/s)                          |                                             |
| Travel speed (mm/s)                             | 350                                         |
| Travel speed (IN/s)                             | 13.78                                       |
|                                                 |                                             |
| **Size of printed build**                       |                                             |
| Max build volume (liter)                        | 12.4                                        |
| Max print dimensions single extruder (x,y,z) (mm) | 230 x 270 x 200                             |
| Max print dimensions single extruder (x,y,z) (IN) | 9 x 10.6 x 7.8                              |

*(source:
[https://www.lpfrg.com/en/products/printer-compare/creatr-1/](https://www.lpfrg.com/en/products/printer-compare/creatr-1/))*
