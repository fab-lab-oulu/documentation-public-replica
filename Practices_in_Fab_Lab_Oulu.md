# Practices in Fab Lab Oulu

**This page describes common practices in Fab Lab Oulu with regard to opening hours, and the use of Fab Lab Oulu facilities and materials.**

## Opening hours

Fab Lab is open to everybody. The following opening hours apply:

* For University of Oulu students and staff: Mon -Fri, 9.00 * 16.00.
* For general public: Mon, 9.00 * 18.00.

For update on seasonal opening schedule please consult the [main page](http://www.oulu.fi/fablab/node/32345).

## Register yourself as user of Fab Lab Oulu

* We recommend you register yourself as a user of the Fab Lab in order to be able to book the machines and keep track of your experience with the machines.
* Go to [http://fablab.virtues.fi/fablab/](http://fablab.virtues.fi/fablab/) and register.
* If you are a member of the University of Oulu (student or staff), please use your University account e-mail address when you enter your information in the online system [http://fablab.virtues.fi/fablab/](http://fablab.virtues.fi/fablab/) . This ensures that you'll have access rights to the Fab Lab throughout the entire week from Monday to Friday.

## Reservation of machines

* We have an online reservation system where you can make reservations of the machines: [http://fablab.virtues.fi/fablab/.](http://fablab.virtues.fi/fablab/)
* In order to make sure the machines you want to use are available, please check the availability of the machines at our online reservation system.
* If you are a new user, please visit us first and learn using the machine before making any new reservation.
* Before making a reservation, please check the necessary time required of the machine for your work.

## About using the machines

* When first time in Fab Lab, please ask the staff to provide a guided tour.
* Before using any machine for the first time, ask for training from Fab Lab staff.
* Never leave a machine unattended.
* Read and follow the safety rules to keep yourself, the machines, and the space safe.

## About the use of materials

* Students and public participating in various training and events at Fab Lab Oulu can use the materials for free, unless otherwise agreed upon.
* **Read the instructions on what type of materials can be handled by the Fab Lab Oulu machines and processes at their corresponding pages. Ask Fab Lab Oulu staff for further information and advice on the use of materials.** Below are some general guidelines.
* Regarding the laser cutter:
* The Fab Lab Oulu laser cutter is not suitable for certain materials such as PVC and others. [Consult this list with allowed and not allowed materials](Materials_allowed_not_allowed_with_the_laser_cutter). Ask Fab Lab Oulu staff for further advice on the use of materials.
    + Do not use unknown materials with the laser cutter.
    + The general practice is that everyone using the laser cutter brings their own material.
    + Fab Lab Oulu provides some materials to be used freely such as cardboard by the University students and staff and general public.
    + Some other available materials such as plywood and acrylic can also be used freely in small amounts by the users of Fab Lab Oulu.
* Regarding the 3D printers: + The 3D printers use materials available in the Fab Lab Oulu. Ask Fab Lab staff if you have own material (e.g., certain color ABS or PLA). The LeapFrog 3D printer is free to use by the University and the general public when parts of reasonable size and amount are printed. + The Stratasys 3D printer is usually invoiced, except for small prints by our students, when industrial quality is needed. Ask Fab Lab staff for further information on using the Stratasys 3D printer.
* Regarding the electronics production equipment:
* PCB boards are free to use by everybody if consumed in reasonable amounts.
* Electronics components that are available in Fab Lab Oulu (mostly surface-mount components) are free to use by the University.
* Materials such as soldering paste and others are free to use by anyone, but please ask first Fab Lab Oulu staff.
* Regarding the use of computers and the meeting space (Parvi): + Six computers are available to use for the design of 2D and 3D models by anyone. + The computers and meeting space can be used freely in the limit of availability.
