/**
  Copyright (C) 2015-2021 by Autodesk, Inc.
  All rights reserved.
*/

description = "Export as svg";
vendor = "Epilog Laser";
vendorUrl = "https://www.epiloglaser.com";
legal = "Copyright (C) 2015-2021 by Autodesk, Inc.";
certificationLevel = 2;

longDescription = "Custom post for Epilog Laser. The post will output a .SVG file which has to be converted to .PDF for printing. " +
  "You can do both through-cutting and etching in the same program by enabling the 'useColorMapping' property. " +
  "When enabled, RED (255,0,0) will be used for through-cutting and GREEN (0,255,0) for etching. " +
  "You have to make sure to turn on color mapping and adjust the power settings correspondingly in the Epilog settings. " +
  "By default you will get BLUE (0,0,255) for both through-cutting and etching.";

extension = "svg";
mimetype = "image/svg+xml";
setCodePage("utf-8");

capabilities = CAPABILITY_JET;

minimumCircularSweep = toRad(0.01);
maximumCircularSweep = toRad(180); // avoid potential center calculation errors for CNC
allowHelicalMoves = false;
allowedCircularPlanes = (1 << PLANE_XY); // only XY arcs

properties = {
  margin: {
    title: "Margin(mm)",
    description: "Sets the margin in mm.",
    type: "number",
    value: 1,
    scope: "post"
  },
  outputGuide: {
    title: "Output guide",
    description: "Enable to include help geometry.",
    type: "boolean",
    value: false,
    scope: "post"
  },
  useColorMapping: {
    title: "Use color mapping",
    description: "Enable to use color mapping.",
    type: "boolean",
    value: false,
    scope: "post"
  }
};

var xyzFormat = createFormat({ decimals: (unit == MM ? 3 : 4) });

// Recommended colors for color mapping.
var RED = "rgb(255,0,0)";
var GREEN = "rgb(0,255,0)";
var BLUE = "rgb(0,0,255)";
var YELLOW = "rgb(255,255,0)";
var MAGENTA = "rgb(255,0,255)";
var CYAN = "rgb(0,255,255)";

/** Returns the given spatial value in MM. */
function toMM(value) {
  return value * ((unit == IN) ? 25.4 : 1);
}

function onOpen() {
  writeln("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");

  if (getProperty("margin") < 0) {
    error(localize("Margin must be 0 or positive."));
    return;
  }

  var box = getWorkpiece();
  var width = toMM(box.upper.x - box.lower.x) + 2 * getProperty("margin");
  var height = toMM(box.upper.y - box.lower.y) + 2 * getProperty("margin");

  writeln("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"" +
    xyzFormat.format(width) + "mm\" height=\"" + xyzFormat.format(height) +
    "mm\" viewBox=\"0 0 " + xyzFormat.format(width) + " " + xyzFormat.format(height) +
    "\">");


  // we output in mm always so scale from inches
  xyzFormat = createFormat({ decimals: (unit == MM ? 3 : 4), scale: (unit == MM) ? 1 : 25.4 });
}

function onComment(text) {
}

// From manual: Lines that you want to engrave rather than cut should be set .006" or greater.
var cuttingWidth = 0.02; // make sure we do vector cutting - and not raster - from manual - max 0.03"
var cuttingColor = BLUE; // color determines the cutting mode
var guideWidth = 0.5; // make sure this gets ignored
var guideColor = MAGENTA; // make sure this gets ignored

function onSection() {

  switch (tool.type) {
    case TOOL_WATER_JET: // allow any way for Epilog
      warning(localize("Using waterjet cutter but allowing it anyway."));
      break;
    case TOOL_LASER_CUTTER:
      break;
    case TOOL_PLASMA_CUTTER: // allow any way for Epilog
      warning(localize("Using plasma cutter but allowing it anyway."));
      break;

    default:
      error(localize("The CNC does not support the required tool."));
      return;
  }

  switch (currentSection.jetMode) {
    case JET_MODE_THROUGH:
      cuttingColor = getProperty("useColorMapping") ? RED : BLUE;
      break;
    case JET_MODE_ETCHING:
      cuttingColor = getProperty("useColorMapping") ? GREEN : BLUE;
      break;
    case JET_MODE_VAPORIZE:
      cuttingColor = getProperty("useColorMapping") ? GREEN : BLUE;
      warning(localize("Using unsupported vaporization. Using etching instead."));
      break;
    default:
      error(localize("Unsupported cutting mode."));
      return;
  }

  var remaining = currentSection.workPlane;
  if (!isSameDirection(remaining.forward, new Vector(0, 0, 1))) {
    error(localize("Tool orientation is not supported."));
    return;
  }
  setRotation(remaining);
}

function onParameter(name, value) {
}

function onDwell(seconds) {
}

function onCycle() {
}

function onCyclePoint(x, y, z) {
}

function onCycleEnd() {
}

function writeLine(x, y) {
  if (radiusCompensation != RADIUS_COMPENSATION_OFF) {
    error(localize("Compensation in control is not supported."));
    return;
  }

  switch (movement) {
    case MOVEMENT_CUTTING:
    case MOVEMENT_REDUCED:
    case MOVEMENT_FINISH_CUTTING:
      break;
    case MOVEMENT_RAPID:
    case MOVEMENT_HIGH_FEED:
    case MOVEMENT_LEAD_IN:
    case MOVEMENT_LEAD_OUT:
    case MOVEMENT_LINK_TRANSITION:
    case MOVEMENT_LINK_DIRECT:
    default:
      return; // skip
  }

  var start = getCurrentPosition();
  if ((xyzFormat.format(start.x) == xyzFormat.format(x)) &&
    (xyzFormat.format(start.y) == xyzFormat.format(y))) {
    return; // ignore vertical
  }
  writeln("<line x1=\"" + xyzFormat.format(start.x) + "\" y1=\"" +
    xyzFormat.format(-start.y) + "\" x2=\"" + xyzFormat.format(x) +
    "\" y2=\"" + xyzFormat.format(-y) + "\" fill=\"none\" stroke=\"" +
    cuttingColor + "\" stroke-width=\"" + cuttingWidth + "\"/>");
  if (getProperty("outputGuide")) {
    writeln("<line x1=\"" + xyzFormat.format(start.x) + "\" y1=\"" +
      xyzFormat.format(-start.y) + "\" x2=\"" + xyzFormat.format(x) +
      "\" y2=\"" + xyzFormat.format(-y) + "\" fill=\"none\" stroke=\"" +
      guideColor + "\" stroke-width=\"" + guideWidth + "\"/>");
  }
}

function onRapid(x, y, z) {
  writeLine(x, y);
}

function onLinear(x, y, z, feed) {
  writeLine(x, y);
}

function onRapid5D(x, y, z, dx, dy, dz) {
  onExpandedRapid(x, y, z);
}

function onLinear5D(x, y, z, dx, dy, dz, feed) {
  onExpandedLinear(x, y, z);
}

function onCircular(clockwise, cx, cy, cz, x, y, z, feed) {
  if (radiusCompensation != RADIUS_COMPENSATION_OFF) {
    error(localize("Compensation in control is not supported."));
    return;
  }

  switch (movement) {
    case MOVEMENT_CUTTING:
    case MOVEMENT_REDUCED:
    case MOVEMENT_FINISH_CUTTING:
      break;
    case MOVEMENT_RAPID:
    case MOVEMENT_HIGH_FEED:
    case MOVEMENT_LEAD_IN:
    case MOVEMENT_LEAD_OUT:
    case MOVEMENT_LINK_TRANSITION:
    case MOVEMENT_LINK_DIRECT:
    default:
      return; // skip
  }

  var start = getCurrentPosition();

  var largeArc = (getCircularSweep() > Math.PI) ? 1 : 0;
  var sweepFlag = isClockwise() ? 1 : 0;
  var d = [
    "M", xyzFormat.format(start.x), xyzFormat.format(-start.y),
    "A", xyzFormat.format(getCircularRadius()), xyzFormat.format(getCircularRadius()),
    0, largeArc, sweepFlag, xyzFormat.format(x), xyzFormat.format(-y)
  ].join(" ");
  writeln("<path d=\"" + d + "\" fill=\"none\" stroke=\"" +
    cuttingColor + "\" stroke-width=\"" + cuttingWidth + "\"/>");
  if (getProperty("outputGuide")) {
    writeln("<path d=\"" + d + "\" fill=\"none\" stroke=\"" +
      guideColor + "\" stroke-width=\"" + guideWidth + "\"/>");
  }
}

function onCommand() {
}

function onSectionEnd() {
}

function onClose() {
  writeln("</svg>");
}

function setProperty(property, value) {
  properties[property].current = value;
}
