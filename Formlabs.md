# Formlabs

## **Formlabs 3D printer** - for high-precision, small-scale 3D models

### Model: **Form 2**

![](https://static.formlabs.com/static/formlabs-web-frontend/img/bessy/products/tech-specs/F2-empty-257px.jpg?cb87801g6b0b1d7g82117e2)

Vendor website: [https://formlabs.com/3d-printers/form-2/](https://formlabs.com/3d-printers/form-2/)

#### Technical specifications

|                        |                          |                                             |
|------------------------|--------------------------|---------------------------------------------|
| **Hardware**           | **Dimensions**           | 35 × 33 × 52 cm<br>13.5 × 13 × 20.5 in      |
|                        | **Weight**               | 13 kg<br>28.5 lbs                           |
|                        | **Operating Temperature**| Auto-heats to 35° C<br>Auto-heats to 95° F  |
|                        | **Temperature Control**  | Self-heating Resin Tank                     |
|                        | **Power Requirements**   | 100–240 V<br>1.5 A 50/60 Hz<br>65 W         |
|                        | **Laser Specifications** | EN 60825-1:2007 certified<br>Class 1 Laser Product<br>405nm violet laser<br>250mW laser |
|                        | **Connectivity**         | Wifi, Ethernet and USB                      |
|                        | **Optical Path**         | Protected                                   |
|                        | **Printer control**      | Interactive Touch-screen with Push-button   |
| **Printing Properties**| **Technology**           | Stereolithography (SLA)                     |
|                        | **Peel Mechanism**       | Sliding Peel Process with wiper             |
|                        | **Resin Fill System**    | Automated                                   |
|                        | **Build Volume**         | 145 × 145 × 175 mm<br>5.7 × 5.7 × 6.9 in    |
|                        | **Layer Thickness (Axis Resolution)** | 25, 50, 100 microns<br>0.001, 0.002, 0.004 inches |
|                        | **Laser Spot Size (FWHM)** | 140 microns<br>0.0055 inches              |
|                        | **Supports**             | Auto-Generated<br>Easily Removable          |
| **Material Properties**| **Packaging**            | 1 L cartridges                              |
|                        | **Resin Material Properties** | [See More Information](https://formlabs.com/materials/)<br>[Castable Resin Burnout Process (PDF)](https://formlabs.com/media/upload/Castable-Resin-Burnout-Process.pdf)<br>[Clear Resin Datasheet (PDF)](https://formlabs.com/media/upload/Clear-DataSheet-v2.pdf)<br>[Dental Model Data Sheet (PDF)](https://formlabs.com/media/upload/DentalModel-DataSheet.pdf)<br>[Dental SG Resin Datasheet (PDF)](https://formlabs.com/media/upload/DentalSG-DataSheet.pdf)<br>[Durable Resin Datasheet (PDF)](https://formlabs.com/media/upload/Durable-DataSheet-201.pdf)<br>[Flexible Resin Datasheet (PDF)](https://formlabs.com/media/upload/Flexible-DataSheet_D93ECMO.pdf)<br>[High Temp Resin Datasheet (PDF)](https://formlabs.com/media/upload/HighTemp-DataSheet.pdf)<br>[Tough Resin Datasheet (PDF)](https://formlabs.com/media/upload/Tough-DataSheet.pdf)<br>[Full Materials Datasheet (PDF)](https://formlabs.com/media/upload/XL-DataSheet-201.pdf) |
|                        | **Material safety data sheets** | [Black Resin SDS (PDF)](https://formlabs.com/media/upload/Black-SDS_miI3Oqs.pdf)<br>[Castable Resin SDS (PDF)](https://formlabs.com/media/upload/Castable-SDS_yzC8bun.pdf)<br>[Clear Resin SDS (PDF)](https://formlabs.com/media/upload/Clear-SDS_u324bsC.pdf)<br>[Dental Model SDS (PDF)](https://formlabs.com/media/upload/DentalModel-SDS.pdf)<br>[Dental SG Resin SDS (PDF)](https://formlabs.com/media/upload/DentalSG-SDS.pdf)<br>[Durable Resin SDS (PDF)](https://formlabs.com/media/upload/Durable-SDS-201.pdf)<br>[Flexible Resin SDS (PDF)](https://formlabs.com/media/upload/Flexible-SDS_eSMv2T6.pdf)<br>[Grey Resin SDS (PDF)](https://formlabs.com/media/upload/Grey-SDS_lQLbyNE.pdf)<br>[High Temp Resin SDS (PDF)](https://formlabs.com/media/upload/HighTemp-SDS.pdf)<br>[Tough Resin SDS (PDF)](https://formlabs.com/media/upload/Tough-SDS.pdf)<br>[White Resin SDS (PDF)](https://formlabs.com/media/upload/White-SDS_H6IAHUS.pdf) |
| **PreForm Software**   | **System Requirements**  | - Windows 7 and up<br>- Mac OS X 10.7 and up<br>- OpenGL 2.1<br>- 2GB RAM<br>[Learn more](https://formlabs.com/tools/preform/) |
|                        | **Hardware Requirements**| Formlabs Form 2 3D Printer                  |
|                        | **Features**             | - Simple print setup<br>- Auto-orient for optimal print position<br>- Auto-mesh repair<br>- Auto-generation of supports<br>- Rotation, scaling and duplication<br>- Layer slicer for path inspection<br>- .STL and .OBJ file input<br>- .FORM file output |
| **Finish Kit**         | **Includes**             | - Finishing Tray<br>- Scraper<br>- Pre and Post-Rinse Tubs<br>- Rinse Basket<br>- Squeeze Bottle<br>- Flush Cutters<br>- Tweezers<br>- Disposable Nitrile Gloves<br>- PEC*PAD Wipes<br>- Microfiber cloth<br>- Removal Tool<br>- Removal Jig |

Read about design-specs:
[https://formlabs.com/3d-printers/design-specs/.](https://formlabs.com/3d-printers/design-specs/)

See [guidelines on 3D modeling.](Computer-aided_3D_design)

See [guidelines on 3D printing with Form 2 printer](Printing_with_Formlabs_Form_2).
