# Electronics design and production

This process consists of the following steps:

Planning -> Electronics design -> Milling -> Stuffing -> Testing

(The next stage in digital fabrication is Embedded Programming. See the
page Embedded Programming for further information (to be added in the
future!!!)

**For a more detailed description of the milling and stuffing please see
the corresponding pages.**

## Preliminaries:

[Safety rules in Fab Lab](Safety_rules_in_Fab_Lab_Oulu)  (Read this!)

Software installations:

- Eagle (Go to <https://cadsoft.io/> and install the Express free version)

- GIMP (see <https://www.gimp.org/>)

Documentation:

External resources (Fab Academy, Sparkfun, Eagle tutorials, …)

YouTube tutorials: e.g., Schematic design in Eagle;and Board layout in
Eagle  - these tutorials are for an older version of Eagle (6.2), but
they are quite useful otherwise, with useful tips and information.

Fab Lab Oulu resources

- This Wiki space - which is now "work in progress" and keeps adding new pages for documentation of Fab Lab Oulu processes, machines, and practices

- Fab Academy student pages:
    [Antti](http://archive.fabacademy.org/archives/2016/fablaboulu/students/161/),
    [Juha](http://archive.fabacademy.org/archives/2016/fablaboulu/students/189/),
    [Dorina](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/),
    [Jani](index)

This tutorial is based on:

- <http://archive.fabacademy.org/archives/2016/doc/electronics_design_eagle.html>

- <http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/week6.html>

Other learning resources:

Some tutorials on using Eagle:

- <https://www.sparkfun.com/EAGLE>

- <https://learn.sparkfun.com/tutorials/using-eagle-schematic>

- <https://learn.sparkfun.com/tutorials/using-eagle-board-layout>

- <http://archive.fabacademy.org/archives/2016/doc/electronics_design_eagle.html>

Fab Academy tutorials:

<http://archive.fabacademy.org/archives/2016/doc/electronics_production_FabISP.html>

Video lecture on electronics design in 2016 :
<https://vimeo.com/157476225>

- Includes also components and symbols in schematics

Video lecture on electronics production: <https://vimeo.com/155713990>

Fab academy summary lectures archive in 2016:
<http://archive.fabacademy.org/archives/2016/master/schedule.html>

Fab academy video lectures archive in 2016:
http://archive.fabacademy.org/archives/2016/master/videos/index.html

---

## 1. Planning

- What is my project about?

- How many PCB are needed?

- What the PCB will do?

- What components are needed?

- What components are available?

For the demonstration lesson and hands-on exercise, we create a LED &
BUTTON board as shown below:

<img src="https://lh5.googleusercontent.com/5xaw0VaQwv7cXE5jHzmTn9FjRuXsDieTB8hs7OX_twtHaYjHCJnDJiXzqzNJ-0Flz_0R3IXFboMzeTghZCm3vX_mygMefNOW-HR2OFUS8DmJyf-trW4xkZGm2MnwpoyLmnnj7nka" width="624" height="363" alt="led and button added to hello echo" />

(Picture from
<http://archive.fabacademy.org/archives/2016/doc/electronics_design_eagle.html>)

The components needed are:

- 6-pin programming header: for programming the board

- Microcontroller: ATtiny44A. Once the microcontroller is programmed, the program is stored in non-volatile memory. This means that it will remember the program. (See [ATtiny44A datasheet](http://www.atmel.com/images/doc8183.pdf)).

- FTDI header: powers the board and allows board to talk to computer.
    (see [FTDI cable datasheet](http://www.ftdichip.com/Support/Documents/DataSheets/Cables/DS_TTL-232R_CABLES.pdf))

- 20MHz resonator: external clock. The ATtiny has a 8Mhz clock but the resonator is faster (increase the clock speed of the processor) and more accurate.

- Capacitor 1uF

- Two Resistors 10k Ohm
    + Purpose: pull-up resistor.
        (see:[http://www.sparkfun.com/tutorials/218)](http://www.sparkfun.com/tutorials/218)

- Button (OMERON switch)

- LED (Light Emitting Diode) - LEDs have polarity - the side with the line is the cathode and connects to the ground side.(see schematic below)  

- Resistor (value 499 ohms)

Purpose: current limiting resistor

Why do we need a current limiting resistor? So we don't burn out the
LED.

The LEDs we are using are rated for:

- The typical forward voltage is: 1.8V (e.g., red led has 1.8 V, green led has 2.1 V)

- The max current the LED can take is 20mA.

See how to automatically calculate the needed
resistor:[http://led.linear1.org/1led.wiz](http://led.linear1.org/1led.wiz)

- The calculator gives the minimum resistor value which corresponds to the brightest light; usually we choose a larger value for resistor.

Look at the datasheet for the LED to get the values to plug in: [e.g.,
for red diffused:
Datasheets/](https://learn.adafruit.com/all-about-leds/the-led-datasheet)

When using a switch connected to a microcontroller, there appears a
problem called bouncing which reflects the fact that the transition from
logic high (unpressed button) to logic low (pressed button) is not
stable, but it takes some ms to stabilize. One solution to this problem
is to add more hardware components, i.e., a resistor and a capacitor to
overcome the fluctuation from logic high to logic low. See
[here](http://hackaday.com/2015/12/09/embed-with-elliot-debounce-your-noisy-buttons-part-i/)
about bouncing and debouncing.

In our circuit, one solution suggested by Antti Mäntyniemi is to use the
following components that connect the switch to the microcontroller:

- One Resistor R1 of 49,9 K to connect the button to the power

- One Resistor R2 of 49,9 K to connect the button to the microcontroller pin

- One Capacitor of 1 nF = 1000 pF to ground the button and R2

In addition, we can add one more LED that is not connected to the
microcontroller so that when we power the PCB the LED will be on. The
following components will be needed:

- LED (e.g., green) & Resistor 499 ohm

## 2. Electronics design

- How to connect the components on the PCB (how to draw the circuit)?

We use the open-source software Eagle to create the schematics and board
layout.

Then we use GIMP to create the traces and cutout .png files to be used
in Fab modules to obtain the .rml files for milling.

### The overview of the process

Schematic -> Board layout -> .png image export -> Traces and cutout .png files in GIMP

(after which the next steps are -> Creating the milling files (.rml)
with Fab modules -> Milling with Roland SRM-20 machine;  these steps
are described in the next stage called Milling)

#### Installation of the software Eagle:

Go to http://www.autodesk.com/products/eagle/overview   and install the
free version of Eagle.

Exmaples of tutorials on Eagle: (see also other tutorials at
http://www.autodesk.com/products/eagle/overview )

- <http://archive.fabacademy.org/archives/2016/doc/electronics_design_eagle.html>

- <https://www.sparkfun.com/EAGLE>

- <https://learn.sparkfun.com/tutorials/using-eagle-schematic>

- <https://learn.sparkfun.com/tutorials/using-eagle-board-layout>

- YouTube tutorials

#### Eagle tip and tricks

- In Eagle you can use commands from Menu, from top and left-hand toolbars, or from command line. Move the mouse over the icons on the toolbars to see the names and what they do.

- Do not place anything below or to the left of the cross (origin) in either schematic or layout when you use the free version of Eagle.

- To change the background color of the schematic/layout, from Options > User Interface...

- You can use Autorouter to obtain automatic routes for the circuit.

#### Creating the schematic

- Download and install component libraries (e.g., [fab.lbr](attachments/68814120/73695947.lbr))

- Create a new project

- Create new schematic

- Add components from fab.lbr library and/or other libraries (e.g., supply1)

- Connect the components using Net button

- Check the consistency of electronic rules

The final schematics should look like this (or similar):

<img src="https://lh4.googleusercontent.com/S0O3AITLgXP7CZmriGaX7vPhcNTUzFyYVQM6jN9I9JSttj_im2TF0MaoDAYgdwdTwf3IGBzWMCPsnH9l-7MR59DzgdhzxkzcmV4e0Y87fO3ubD1rzgNME6YkcQxnQxxr5v16XHae" width="900" />

#### To download and install the library fab.lbr:

- Save the library file [fab.lbr](index) (from the Electronics design lecture page in Fab Academy) into the Eagle local folder, e.g., C:\\EAGLE-7.5.0\\lbr). This library contains all the components needed to design and produce the board.

<!-- -->

- To install the library, select the library in the left-hand tree, Right click on it, and select Use.

To create a new folder:

Select the folder/project where you want to create a new folder (in the
left-hand tree Projects > Eagle)

Right click > New Folder

Rename the folder

#### To create a new project

Select the folder where you want to create a new project (in the
left-hand tree Projects > Eagle)

File > New > Project

Rename the project

#### To create a new schematic

Select the project in the left-hand tree

File > New > Schematic

Save the schematic with a descriptive name

#### Add components from fab.lbr library

In the schematic window:

Select the “Add” icon from the left-hand toolbar

Open “fab” library and select the desired component

<img src="https://lh6.googleusercontent.com/xf7ejWbUSvywhO8LniUXbqf2q7gNzmqkFHlXUw9ASXFUG_jH34oc9bWuTq4tSqxHIoXJw5CT_FP3r0scy4yv73jEMDzd6PNcaoP_WF_QDpyfETLqRI1xCShjqUcufhVoDyPPogCH" width="624" height="437" />

<img src="https://lh6.googleusercontent.com/co1vTu0qf5GBdwDcQg5CO1k9qXOFUu4Ah1bfOReF80GOBpANUwIh1helBumJ8G4bHhuVSEG5-5yuSQtDtKAMouYcwAOrFqlHGo2V_YGJCCgr_fY5JAxPlajel3mpdGIDlTFI6M_C" width="415" height="143" />

Press OK

And click on the schematic to place the component.

Later you can move the components using the button “Move”.

<img src="https://lh5.googleusercontent.com/PDFKhz_IHpKiZhrxIAxT_ViO2jHsbkeggcaCQ1Fyk-mT-uD0tGiPA5bbw8awrGzkJfhXN2-H5oqQGjQkHdoN1KdRqp5vMh0GFdnQaTd7YjC-Ijwqjoy7P4HBkOmsuQLUScSDGoVC" width="517" height="400" />

#### To add supply (VCC) and ground (GND) components

Use the library supply1.

#### Connect the components using Net button

Two possibilities:

1. You can connect the components with a continuous wire (called net) by using the button “Net". This is a good solution for relatively simple schematics; it can become messy when nets cross over each other.
1. You can give the same name to the components (e.g., GND, VCC) or nets that are connected. Eagle will ask you if you want them to be connected and say yes. After you name the component - label it so the name appears in the diagram.

For example, in the figure below:

- All of the components in the schematic named GND are connected to a common ground point.

- The button is connected to a net on the button component and pin 10 on the microcontroller.

- The led is connected to a net named LED that links the resistor R2 with the pin 6 on the microprocessor.

<img src="https://lh3.googleusercontent.com/z3fFA27Y6vl03fKxmStDhFpFuCvuH8xYRAWERiLW91iuVXeLS9xBxiHf8ttVP3ihwf65Wdnoj-AcCZnkg4f254a2lxOasRYLAw6MmyzYgQzoB4WlqfrXYSKFPjWTVGlIsBBZJrGG" width="624" height="464" alt="diagram" />

(Picture from
<http://archive.fabacademy.org/archives/2016/doc/electronics_design_eagle.html>)

#### Check the consistency of electronic rules

Click ERC to check the schematic for electronic rules errors

Correct the errors and switch to the board layout design

One model of schematic for the PCB we want to create is the following:

<img src="https://lh5.googleusercontent.com/7KgPQ5n2cD3rLemi-mYq34To9rvS_iMCITpbU3FeOFQ_T1MNjIopiJs1kgdBRBFMKHfM3j7sUyHw799LSe3lQFzEQmXlb8Uqk2bNB9psDL8mTaNU7Ml-I1m2oiDJ0Wzmo1HWcSfN" width="900" />

#### Creating the board layout

- From Schematics view switch to board layout using the
    “Generate/switch to board” button in the top toolbar

- Move components into the board area

- Connect the components using Route button

- Check for consistency (check design rules, DRC)

The final board layout could look like this:

<img src="https://lh5.googleusercontent.com/jZcR0QrvycoQs18EQqWjzTvvyDv20ex18hmfRVpW2zkvauhJmTMbzNAFyZL5enjS3oy5XjYxLu4pVabAfx3tpJkOEZikZMc1goCFfmyAUJPhhYsRzfEC9Qy8ht1-JVMJCBsE3bSX" width="346" height="284" />

Or like this below (when debouncing and the extra LED are included):

<img src="https://lh6.googleusercontent.com/SKn1LlPqHEvY5hVb477DS5euYBdwjs7wHL7eb6J-hSUwl4XqtGyOTCSgnoy-w7MJz4FimZn0eRQA3qnt2XF3CPETDU7zbPjRRgcqqU0Z5kmkOTnCzw1SkeyZ743IJUQiP-cYZRGG" width="590" height="596" />

#### Check for consistency (check design rules, DRC)

Click on DRC icon

Set “Clearance” parameters to 16 mil for Pads and Routes; all other
parameters were set to default values. You can write in the text boxes
16mil and the transformation between units (mil > mm) will be done
automatically. (1 mil = 0,001 inch; 16 mil = 0,4 mm = the diameter of
the milling bit)

#### Tips to fix the problems of overlaps and close connections:

- Change the dimension of the grid (View > Grid… > Alt: decreases the value of the default grid when pressing Alt key)

- Change the width of the wires (select Change tool > Width > choose a smaller value)

#### Export the board layout in png format

Export the top layer design from Eagle into an image .png with
resolution 1500 dpi and monochrome.

View > Layer settings ... > None; 1: Top  (to select only the top
layer of the layout)

and then

File > Export as… and select the parameters in the picture below:

<img src="https://lh3.googleusercontent.com/hW0-h-IZZD7WzCbzwCW-lhIOnVaRu9ifTmmz4A4poQqJVZFj5HP8rdj9lTUg_VRTlGoOJv1JZ1c07vKFm7laTgpEECXh1etouzzHEQf7mBEtbnCoG9vND4S4UeYnSJam-RmEFmSk" width="309" height="213" />

#### Create the traces and cutout png file in Gimp

Two files are created: traces.png and cutout.png which will be used in
Fab modules to generate the files for milling (format .rml). These two
files could look like below examples.

<img src="https://lh5.googleusercontent.com/QHXXgXQuhHjHhVlHVY9S_aBugKLr64nxO3vMZFVylMir9nVUFpiat9gO6JNrIk5gYX78tjrWy_6i8vK3KxD9LExNXvBSMgSNhnpCwsTxvdnT6BZfbYH6yXNTvmy4THAEEokHfXqJ" width="624" height="257" />

Both images should have the same size and to ensure this we use Gimp to
create the two image files.

The steps are:

1. Open the image in Gimp and create a new layer with foreground color black. This new layer is for the outline. The dimensions below are for illustration only.

<img src="https://lh3.googleusercontent.com/qh3aIZXxixpJ_8Y0TIxmLlLzj30lVN3j4aaVkUux6pnEoGrQN2oFn5GgwDc9H2h76HpyuTGQbkyE5u94kEbmFDg9XWQLci2jjjorAcocF_gfnADi-OIx4mQ5f6CQM_rDXwA-QWmK" width="243" height="288" />

2\. Set Layer Boundary Size for the outline layer by adding 1.6 mm to
width and height centered. This is the margin milled away by the machine
with the 1/32 bit (2,54 inch/32 = 0.8 mm).

<img src="https://lh5.googleusercontent.com/JKbR36Yx1vX64GdrBXsbPFh6N27Hu8gnzdpcq4fQi-3I8iiUalZXPWFEbOZln9OdyECex2x9k9fpyQqzB0EH9_yto7wnquBFwJmj7iyi_cWznet-cE3V8elZ83yC84FJ16BuDZ0H" class="image-left" width="554" height="363" />

3\. Change the order of the layers in the Layers tab: on top the traces
layer, and below the outline layer.

<img src="https://lh3.googleusercontent.com/8sskrDvXP3TiHmmcRFluj7UnABOU1MIK57LwplV7RtIBRNQ9eD66j2_I3JS-Sp8hwVSlaTQzggc8laUFCtey6905B_7_F3cdyMqXAhBwCXebs0Hal337cnFoE1W1-YFabO30dD7X" width="624" height="357" />

4\. Enlarge the image to fit all layers: Image > Fit canvas to
layers.

Then fill the empty area with black.

5\. To create the traces image file, export the image as .png.

<img src="https://lh6.googleusercontent.com/HKxCPZyGJQzyjcI6K-0mxijIXjS8Mqr42VLPX7sc_-J9soiZOfINQvnP7-sGMRMMORrhnJADbEHnmZg4Egmt-tb2gmNU8VJQla9VFhn-FprJ5sE8zgtajdTQ3bjl_67iSoaU8zqd" width="335" height="335" />

6\. To create the cutout image file, fill the traces layer with white
and export the obtained image as .png.

<img src="https://lh4.googleusercontent.com/G6T1evsZOK1Divd70zOXklgV2jLWkViCzh8UCoEpIp7LmIWzrqtY1NjG3nonNUE_zQSDcxjFu8J-QIVBs_AtUR5TfkrxlBIg_AFu-REWa7W8NRQB3ELs8gIDXyKZj2yuYAc6xqBG" width="367" height="367" />

These two files (traces.png and cutout.png) will be used in Fab modules
(fabmodules.org) to create the .rml files used by the milling machine
Roland SRM-20.

## 3. Milling

### Process

For milling, we use Roland SRM-20 machine.

We create the milling files with Fabmodules as follows

Open <http://fabmodules.org>

Select as input format image (.png)

<img src="https://lh6.googleusercontent.com/ygA2hVvl6Z995NuPOl-bcBwFF_yn6FUh6xup8UXogINSj-lpCsYAklUMVGniC5qfcCszlshx4zVFe8D2biGZCjyKml3fLuY604XoWaIiD67D5tuPJpghBYjQRc0a11bjrQH0I8zr" width="150" />----->>
  <img src="https://lh3.googleusercontent.com/Ywv9UzbCF45DXxeiGpE09jPhawA30KqTgC8N1XJuqN5Q64p75SlaI8waHG7GbZBREXdr0ohtVHWBDO9Ei47yMSQfb7BsbIG8vb1U6VDGozfRLk7CUU8Zd8DRTWdDU8DjR57KIYP-" width="200" />

Select from your folders the file you want to process (traces.png or
cutout.png).

Select as output format Roland mill (.rml)

<img src="https://lh3.googleusercontent.com/PNpc-8KvyLQVrdGHzRZokXnC-eNYcQF6YdPxaMvTxF44FoPVB3ohmTiPcuV9LLEqkwH5W7Nz6pV5qE2lH053RAtFpb26UU2Fy7LsFZKbau8coN2G_R1fL-BhsD_CPRan8ZQ6t8Uo" width="200" />

#### For creating the traces .rml file:

Select as process PCB traces (1/64). 1/64 is the dimension of the
milling bit in inch (cca 0.4 mm diameter).

<img src="https://lh6.googleusercontent.com/HLxTkxesdKyyqGbvFhDhU0h0C3lriQffyDhHt_EuCoOyyBljZ5IjTwkTCAXunnI7e74v9xsAmfnc0OpW29uU47lSZ7FHl1FK0wvj77YcYtJhapEBWq2MF57jnFNEChxlSOvLLVFV" width="150" />

Then in the right-hand column change the parameters marked with red
below:

<img src="https://lh6.googleusercontent.com/lENklGKf5uSBRYqixK2djoBrC4PG1yUEic6sAtzMcJLNjF--hE1iu6CtqZCxU6oV7UJNoZQ5lw1TqjWaBfBpiQIZA_CIBITOxRaw1CoTquAENxZhT8bvbaH8EzzUww6QmenJFvly" width="200" /> The other parameters remain with the default values, except the
following:

cut depth (mm) = 0.15

Then press “calculate” to obtain the milling paths.

If everything looks good, press “save” to save the .rml file.

#### For creating the cutout .rml file:

Select as process PCB outline (1/32). 1/32 is the dimension of the
milling bit in inch (cca 0.79 mm diameter).

Then in the right-hand column change the parameters marked with red
below:

<img src="https://lh6.googleusercontent.com/lENklGKf5uSBRYqixK2djoBrC4PG1yUEic6sAtzMcJLNjF--hE1iu6CtqZCxU6oV7UJNoZQ5lw1TqjWaBfBpiQIZA_CIBITOxRaw1CoTquAENxZhT8bvbaH8EzzUww6QmenJFvly" width="200" /> The other parameters remain with the default values, except the
following:

stock thickness (mm) = 1.70 (this varies depending on the material used;
for FR4 the recommended value is 1.75)

Then press “calculate” to obtain the milling paths.

If everything looks good, press “save” to save the .rml file.

The milling is also documented on a printed page near the milling
machine.

*At the end of the process, do visual inspection of the board and check
that there are no shorts due to poor milling.*

*Wash the board and sandpaper it for cleaning the dust and the undesired
copper.*

*For practicing milling, you can use as input in Fabmodules the
following files:*

Own files for the board BUTTON&LED that you have designed

Ready-made files for the board BUTTON&LED
([traces.png](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/Traces_ButtonLed.png),
[outline.png](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/Cutout_ButtonLed.png))

Ready-made files for an USBtiny board (ISP programmer board)
([tracesISP.png](http://fab.cba.mit.edu/classes/863.11/people/valentin.heun/JPEG/ISP.png),
[outlineISP.png](http://fab.cba.mit.edu/classes/863.11/people/valentin.heun/JPEG/cutout.png)).
See here more information about ISP programmer boards:

- [What is the FabISP?](http://archive.fabacademy.org/archives/2016/doc/electronics_production_FabISP.html)

- [Valentin circuit design](http://fab.cba.mit.edu/classes/863.11/people/valentin.heun/2.htm)

- David's[documentation](http://fab.cba.mit.edu/content/projects/fabisp/)

Own files for your own project

Other learning resources for milling:

- <http://fab.cba.mit.edu/content/processes/PCB/modela2.html>

## Stuffing

The stuffing (and soldering) are documented on Fab Academy pages (see
Electronics production weeks):

- [Antti](http://archive.fabacademy.org/archives/2016/fablaboulu/students/161/),
    [Juha](http://archive.fabacademy.org/archives/2016/fablaboulu/students/189/),
    [Dorina](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/),
    [Jani](index)

- <http://archive.fabacademy.org/archives/2016/doc/electronics_production_FabISP.html>

- David's[documentation](http://fab.cba.mit.edu/content/projects/fabisp/)

- Video lecture on electronics production:
    <https://vimeo.com/155713990>

In practice, stuffing consists of finding the components and placing
them on the PCB in the right locations and orientation.

For soldering you can use the solder iron, or alternatively solder paste
with pick-and-place system and then solder with reflow oven. Use reflow
oven only if you have used FR4 for the PCB. FR1 is not suitable for the
reflow oven because it will melt.

You need to look at the schematic for identifying the right orientation
of the components (e.g., for leds, microcontroller, zener diode) and at
the labeled board layout to see the place of the components. Print these
two pictures (schematic and labeled board layout) or make screenshots
with your phone and annotate them before you start stuffing.

See example below:

<img src="https://lh3.googleusercontent.com/5_2NFMHoe_7w0EuoZJRokCCm5b1d04CkJuI1AaeFxbxD6aq3q1dQ-f9w0-sNoD7H30zsE1TmFmb2BKk1hIu-gm4cTHu8CB-37Rci6eT_95n3SBeHPS5IXO7lG8_93K27aEMnZAXc" width="475" height="267" />

Rules of thumb for stuffing:

- Start with placing/soldering the smaller components

- Start from the inside of the board to the outside

## 5. Testing

Before powering up the board, you must test for soldering errors and
shorts.

Do visual inspection of the board and check that (use microscope if
necessary):

- Unintended connections are not occurring due to soldering.

Use multimeter to check:

- that ground and power are not connected

- that there is not a short on the power line

- which pins are connected

If there are mistakes of soldering:

- Desolder/resolder the bad connections.

## 6. Programming

Not part of this process, but can be illustrated andpracticed independently.

The steps for programming the board are illustrated
[here](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/week8.html).

To reserve a time for a machine please use fablab.virtues.fi.

## Attachments:

<img src="images/icons/bullet_blue.gif" width="8" height="8" />
[fab.lbr](attachments/68814120/73695947.lbr)
(application/octet-stream)
