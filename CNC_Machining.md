# CNC Machine

## Large Wood Router:

The CAM post processor file for Inventor/Fusion360:
[FabLabCNC.cps](attachments/71336967/71499922.cps)

The file for tooling values for different routing bits:
[FABLAB ROUTERI TOOLS 2018-2.hsmlib](attachments/71336967/74613208.hsmlib)

Instructions for converting ArchiCAD files to Inventor files and CAM
process (in Finnish): [ArchiCad Inventor ohje.pdf](attachments/71336967/72056968.pdf)

## Roland (Small milling machine):

The CAM post processor file for Inventor/Fusion360:
[Roland_iso.cps](uploads/188325768a2a5f1f4a28cf571a28933d/roland_iso.cps)

The file for tooling values for different routing bits:
[roland_tools.hsmlib](uploads/49df1e4353e6a59487a49e3e3bc728c1/roland_tools.hsmlib)

## Attachments:

[FabLabCNC.cps](attachments/71336967/71499922.cps)

[FABLAB ROUTERI TOOLS.hsmlib](attachments/71336967/72056967.hsmlib)

[ArchiCad Inventor ohje.pdf](attachments/71336967/72056968.pdf)

[FABLAB ROUTERI TOOLS 2018.hsmlib](attachments/71336967/73695491.hsmlib)

[FABLAB ROUTERI TOOLS 2018-2.hsmlib](attachments/71336967/73695492.hsmlib)

[FABLAB ROUTERI TOOLS 9-2018.hsmlib](attachments/71336967/74613208.hsmlib)

[roland_iso.cps](uploads/188325768a2a5f1f4a28cf571a28933d/roland_iso.cps)

[roland_tools.hsmlib](uploads/49df1e4353e6a59487a49e3e3bc728c1/roland_tools.hsmlib)
