# GIMP

GIMP is a raster graphics program for creating and editing image files.
It is open source and its acronym derives from GNU Image Manipulation
Program.

Its performance in professional tasks makes it the open source
equivalent for Adobe Photoshop. The images can be exported in different
formats, including .png.

The native format of GIMP is .xcf.

GIMP is very useful in Fab Lab Oulu for adjusting the circuit board
layout image files; in particular, we use GIMP for creating the cutout
files to be used in Fab modules (see [here for details on this process relevant to electronic design and production](Electronics_design_and_production)).

GIMP is a complex image processing program that uses layers (an image is
constructed in a hierarchical way, with many layers). Thus, for editing
some part of the image *one has to select the correct layer*.

To install GIMP, please visit [https://www.gimp.org/](https://www.gimp.org/)

On the official page of GIMP, there are also a series of tutorials (see
[https://www.gimp.org/tutorials/](https://www.gimp.org/tutorials/)).
