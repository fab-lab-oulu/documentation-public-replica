# Vinyl cutter

## Roland CAMM-1 GS-24  - desktop cutter for making labels, signs, stickers, adverts, flexible circuits

### Model: CAMM-1 GS-24

"With a completely redesigned cutting carriage and blade holder, the
GS-24 offers great stability, up to 10x overlap cutting and down force
of up to 350 grams so that you can cut like never before — even on
thick, dense substrates.

The GS-24 is Roland's best desktop cutter ever."

[https://www.rolanddga.com/products/vinyl-cutters/camm-1-gs-24-desktop-vinyl-cutter](https://www.rolanddga.com/products/vinyl-cutters/camm-1-gs-24-desktop-vinyl-cutter)

<img src="https://www.oulu.fi/sites/default/files/vinyylileikkuri.JPG" width="759" height="506" />

Vendor website:
[https://www.rolanddga.com/](https://www.epiloglaser.com/)

### Technical specifications

| **Model** | **GS-24** |
| --- | --- |
| Driving method | Digital control servo motor |
| Cutting method | Media-moving method |
| Loadable material width | 2 to 27-1/2 in. (50 to 700 mm) |
| Maximum cutting area | Width: 22.9 in (584 mm) <br> Length: 984.25 in (25000 mm) |
| Acceptable tool | Special blade for CAMM-1 series |
| Maximum cutting speed | 19.69 in/s (500 mm/s) (all directions) |
| Cutting speed | .4 in to 19.69 in/s (10 to 500 mm/sec) (all directions) |
| Blade force | 30 - 350 gf |
| Mechanical resolution | 0.000492 in./step (0.0125 mm/step) |
| Software resolution | 0.000984 in./step (0.025 mm/step) |
| Distance accuracy(1) | Error of less than ±0.2 % of distance traveled, or ±0.1 mm, whichever is greater |
| Repetition accuracy (1)(2) | ±0.1 mm or less |
| Alignment accuracy for printing and cutting when loading printed material (1) (3) | ±1 mm or less for movement distance of 210 mm or less in material-feed direction and movement distance of 170 mm or less in width direction (Excluding effects of printer and/or material) |
| Interface | USB interface (compliant with Universal Serial Bus Specification Revision 2.0 Full Speed, Connecting Multiple Units) |
| Re-plot memory | 2MB |
| Instruction system | CAMM-GL III |
| Power supply | Dedicated AC adapter |
| Input: | AC 100 to 240 V ±10 % 50/60 Hz 1.7 A |
| Output: | DC 24 V, 2.8 A |
| Power consumption | Approx. 30 W (including AC adapter) |
| Acoustic noise level during operation | 70 dB (A) or less (according to ISO 7779) |
| Acoustic noise level during standby | 40 dB (A) or less (according to ISO 7779) |
| Dimensions | 33.5 in (W) × 12.2 (D) × 9.17 (H) <br> [851 (W) x 310 (D) x 235 (H) mm] <br> With stand 33.5 in (W) X 12.2 (D) X 40.75 (H) <br> [851 (W) x 310 (D) x 1035 mm (H)] |
| Weight | 29.77 lbs. [13.5 kg] <br> With stand 58 lb. [26 kg] |
| Environment | Temperature: 5 to 40°C (41 to 104°F), humidity: 35 to 80 % (no condensation) |
| Included items | AC adapter, power cord, blade, blade holder, roller base, alignment tool, USB cable, Set Up guide |

*1: According to material and cutting conditions as specified by Roland
DG Corp. (using the PNS-24 stand, sold separately). 2: Excluding
material expansion and contraction. Provided that media length is under
1600 mm. 3: Using Roland CutStudio, a laser or inkjet printer having a
resolution of 720 dpi or better. Excluding glossy or laminated material.
Excluding effects of printing distortion due to printer precision and
effects of material expansion, contraction, or warping. Depending on the
ink (black) employed by the printer used, correct sensing may not be
possible.*

See guidelines on [Vinyl cutting](Vinyl_cutting).
