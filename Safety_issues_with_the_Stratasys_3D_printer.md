# Safety issues with the Stratasys 3D printer

The Fortus 380 printer is easy to use and safe.

However, the oven, its components, and the printed parts may be
extremely hot and can cause burns.

## Operating the oven - use leather safety gloves

Therefore, to avoid burns when you use the oven (e.g., when placing the
build sheet on the platen or taking your printed part) wear the leather
safety gloves available in the lab.

**Removing the support by hand - use leather safety gloves and safety
glasses**

When your 3D part is ready the support can be removed by hand and with
the help of available tools (e.g., pliers, cutters). To avoid injuries
use safety gloves and safety glasses. The support material can be brittle and may cause cuts when manually broken off.

**Removing the support by dissolving into cleaning agent - use rubber
gloves and safety glasses resistant to heat and alkaline solutions**

If the support material cannot be removed by hand use the cleaning
solution available in the tank in the Lab to dissolve the unbreakable
support.

When placing your 3D parts in the tank with the cleaning agent wear
rubber gloves and safety glasses. Make sure also your hands are covered
(no skin exposed).

Wash the 3D parts with plenty of water and let them dry.

## Finishing - use soap to wash your hands

After finishing your work with the 3D printer and cleaning the parts
from the support material, always wash your hands with soap.

## See more information

- [safety information for removing the support](http://usglobalimages.stratasys.com/Main/Files/Best%20Practices_BP/BP_FDM_SupportRemoval.pdf)
- [user guide Fortus 380mc](attachments/69799741/69895161.pdf)

## Attachments:

 [Fortus 380mc User Guide.pdf](attachments/69799741/69895161.pdf)
