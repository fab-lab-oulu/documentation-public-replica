# Export SVG from Fusion 360

This will be instructions on how to use Fusion360 for laser cutting in Fablab Oulu Note: This is not a tutorial on how to use Fusion360, however if you can make your design in Fusion360 this is the only resource you will need for exporting your project.

Fusion 360 is free to use for educational purposes for students.

[[_TOC_]]

For advanced users:

You can find the post processor from step 2.
The post processor is written in JavaScript and can be modified if need be.

## 1. Create your design

Create your own design. Please note, this is a tutorial on how to export svg from Fusion 360. It is expected that you already know the basics. You can find lots of tutorials about Fusion360 online. In this tutorial the following piece is used as an example:
![starting point](uploads/fusion360_laser/1_create_design/step_1_design_ready.png)

## 2. Initialize Fusion360 for laser cutting:

You need these two files:

[tool library file](uploads/fusion360_laser/tool_library/fablab_laser.hsmlib)

[post processor file](uploads/fusion360_laser/post_processor/svg_laser.cps)

Import both files to fusion from the corresponding library managers:

![Library managers](uploads/fusion360_laser/2_import_libraries/libraries.png)

![post_library](uploads/fusion360_laser/2_import_libraries/post_library.png)

![tool_library](uploads/fusion360_laser/2_import_libraries/tool_library.png)

The tool library can also be created by yourself, it has only a laser cutter with a kerf of 0.2mm.
You may even need to adjust the kerf to your desired thightness of joints.

## 3. Get design ready for laser cutting

We have our design bodies located like they will be after assembly. We need to get them in one plane and this is the easiest way for any number of bodies. Currently your browser looks probably something like this:

![bodies](uploads/fusion360_laser/2_bodies_to_components/bodies.png)

The bodies have to be changed to components but this wont affect your original design because we do it in a manufacturing model.

Change to manufacturing workspace

![manufacture workspace](uploads/fusion360_laser/2_bodies_to_components/change_to_manufacture_workspace.png)

Choose fabrication toolset

![fabrication toolset](uploads/fusion360_laser/2_bodies_to_components/choose_fabrication_toolset.png)

Create manufacturing model

![manufacturing model](uploads/fusion360_laser/2_bodies_to_components/create_manufacturing_model.png)

Edit manufacturing model

![edit manufacturing model](uploads/fusion360_laser/2_bodies_to_components/edit_manufacturing_model.png)

Click on "New Component"

![new component](uploads/fusion360_laser/2_bodies_to_components/create_new_component.png)

Check "From bodies"

![from bodies](uploads/fusion360_laser/2_bodies_to_components/select_from_bodies.png)

Select all of the bodies

![select all bodies](uploads/fusion360_laser/2_bodies_to_components/select_all_bodies.png)

Now you should have all of the bodies as individual components

![components](uploads/fusion360_laser/2_bodies_to_components/components.png)

## 4. Arrange to plane

Select "Arrange" tool

![select arrange tool](uploads/fusion360_laser/3_arrange_to_plane/select_arrange_tool.png)

Select all of the components

![select all components](uploads/fusion360_laser/3_arrange_to_plane/select_all_components.png)

Select a plane

![select a plane](uploads/fusion360_laser/3_arrange_to_plane/select_plane.png)

Show origin

![show origin](uploads/fusion360_laser/3_arrange_to_plane/show_origin.png)

Select the XY-plane

This will make it easier in later steps.

![select the xy plane](uploads/fusion360_laser/3_arrange_to_plane/select_XY_plane.png)

Set options

1. length and width, appropriately for the size of your design
1. object spacing, 1 mm is good to avoid wasting
1. click preview

![set options](uploads/fusion360_laser/3_arrange_to_plane/set_length_width_spacing.png)

Hide origin

![hide origin](uploads/fusion360_laser/3_arrange_to_plane/hide_origin.png)

Finish editing the manufacturing model

![finish editing](uploads/fusion360_laser/3_arrange_to_plane/finish_edit_manufacturing_model.png)

## 5. Create toolpaths:

Now that the components are arranged, we can create the toolpaths required

Create a new setup

![new setup](uploads/fusion360_laser/4_create_toolpaths/create_new_setup.png)

Set operation to cutting, check orientation set stock point up left

![settings](uploads/fusion360_laser/4_create_toolpaths/set_operation_cutting.png)

You can ignore the warnings, or alternatively select all of the bodies one by one

![ignore warnings](uploads/fusion360_laser/4_create_toolpaths/ignore_warning_or_select_bodies_one_by_one.png)

Select laser cutting

![laser cutting](uploads/fusion360_laser/4_create_toolpaths/select_cutting_2d_profile.png)

Select cutting tool

![cutting tool](uploads/fusion360_laser/4_create_toolpaths/select_cutting_tool.png)

![cutting tool](uploads/fusion360_laser/4_create_toolpaths/select_cutting_tool_from_cloud.png)

Select faces

![select faces](uploads/fusion360_laser/4_create_toolpaths/cutting_select_same_plane_faces.png)

Select one of the faces

![select one face](uploads/fusion360_laser/4_create_toolpaths/cutting_select_one_plane.png)

Select keep sharp corner

![keep sharp corner](uploads/fusion360_laser/4_create_toolpaths/cutting_select_keep_sharp_corner.png)

Deselect lead in, lead out and zero the piercing value

![settings](uploads/fusion360_laser/4_create_toolpaths/cutting_deselect_lead_in_out_zero_piercing.png)

Click ok and Fusion calculates the toolpaths

![click ok](uploads/fusion360_laser/4_create_toolpaths/cutting_generated.png)

## 6. Export (post process):

Right click on the operation you want and select post process

![post process](uploads/fusion360_laser/5_postprocess/right_click_select_post_process.png)

Select the post processor (.cps) file you imported earlier and check the other settings

![NC settings](uploads/fusion360_laser/5_postprocess/NC_program_settings.png)

Click "post" and after a few seconds it should prompt success

![post](uploads/fusion360_laser/5_postprocess/NC_code_successfully_posted.png)

## 7. Convert svg to pdf using Inkscape

Open the .svg in Inkscape

Here you can still make final adjustments and such.

![svg file](uploads/fusion360_laser/6_svg_to_pdf/svg_in_inkscape.png)

Save a copy (ctrl + shift + alt + S)

![copy as pdf](uploads/fusion360_laser/6_svg_to_pdf/inkscape_save_a_copy.png)

Select .pdf

![pdf](uploads/fusion360_laser/6_svg_to_pdf/inkscape_select_pdf.png)

Click ok

![click ok](uploads/fusion360_laser/6_svg_to_pdf/inkscape_save_a_copy_as_pdf.png)
