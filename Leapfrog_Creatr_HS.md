# Leapfrog Creatr HS

**IMPORTANT NOTE:** Students and research staff from Architecture have preference in using this machine.

![](images/leapfrog_creatr.jpg)

**Leapfrog Creatr HS** is a good printer to create to create bigger,
faster and stronger 3D prints. It comes with two extruders, which allows
you to print in two different colors at the same time, or even use
soluble support material.

## Technical specifications:

![](attachments/76318112/76611818.png)

More info in the manufacturer's
site: https://www.lpfrg.com/en/3d-printers/

## Usage instructions:

1. [Unpack and run the Slice3r](attachments/76318112/78807249.dmg), cancel on the inital setup.
1. From File&gt;menu, "*load config bundle*" and select [this .ini file](attachments/76318112/78807250.ini).
1. Now you have proper basic settings for running the Leapfrog Creatr HS printer.
1. Next drag and drop the STL file you want to print, or load it from the menu
1. Pick one of the different profiles that have been ready made:
    1. **Fast - Low Detail:** layer thickness 0.3mm, fastest reasonable quality the printer can do
    1. **Fast - Low Detail - Vase:** In this mode, the printer does just one single 0.5mm thick wall, in one continous run.
    1. **Fast - Medium Detail :** layer thickness is 0.2mm, the print takes a bit more time but looks better.
    1. **Slow - Medium Detail:** Layer thickness is still 0.2mm, but the print takes a longer time to finish, giving the filament more time to settle into place properly. Some features of the print will look better with this.
    1. **Slow - High Detail:** Layer thickness is 0.1mm, this setting is the one that will take most time to complete.
1. Beneath the selection for the materials, and profile, there are three buttons, by selecting "**export G-code**" you generate the file that is needed by the printer.
1. Put this file into an USB stick, it is best to keep the clutter on the stick to a minimum, as the menu in the printer interface is slow to read the stick, and can take a while to find the file you want to print if your stick is a mess (so try to use a clean memory stick)
1. After generating the file, the program gives you an educated estimate of the print duration, while you can adjust the details for the profiles, in general it is not that necessary unless you know what you are doing. You can influence the print duration some by tweaking the percentage of the infill, lower the number lesser the filling inside of the item is. Usually 20% is plenty, while for some decorative items even 5% could be enough. Smaller the percentage for the infill faster the print as it takes less work to print.
1. **The maximum size for the object is 250mm wide, 280mm long and
    180mm tall.**
1. While you can set multiple items on the bed, there can be some spider-webbing left behind by the print-head as it moves between the items while printing. So if quality is vital, print one item at a time.

## Attachments:

[PrusaSlicer-2.1.0+-201909160907.dmg](attachments/76318112/78807249.dmg)

[PrusaSlicer_bundle.ini](attachments/76318112/78807250.ini)
