# Computer and meeting space (Parvi)

The **computer and meeting space** provides the following facilities:

- white board
- large screen for presentations and video conferences
- 6 computers

The capacity of the room is about **20 persons**.

**Software installed** on the machines:

- Inkscape (open source vector drawing tool)
- GIMP (open source raster drawing tool)
- Eagle (open source electronics desing tool)
- SolidWorks (commercial 3D CAD tool)
- Autodesk (commerical 3D CAD tool)
- Google Chrome (Internet browser)
