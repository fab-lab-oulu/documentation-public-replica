# Digital Embroidery

You can watch a nice video on how to use our Bernina B550 digital embroidery:

- [Bernina B550 embroidery tutorial](https://youtu.be/s4Ob6PeKfLM)
- [Documentation on how to create a desing with SEWart (Marta Cortés)](https://fabacademy.org/2018/labs/fablaboulu/students/marta-cortesorduna/assignment17.html)
- [Brief documentation on how to transform a svg into stitch file using Bernina software(Arash Satari)](https://fabacademy.org/2019/labs/oulu/students/arash-sattari/assignments/week17.html)
- [Brief documentation on how to transform a svg into stitch file using Bernina software(Jari Laru)](https://fabacademy.org/2020/labs/oulu/students/jari-laru/week18.Wildcard%20Week.html)
- [Presentation](uploads/a096719eaf785901cd2482626dd083ed/Digital_Embroidery.pptx) on different software to use Bernina (Behnaz Norouzi)
