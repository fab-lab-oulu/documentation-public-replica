# Creating a sticker out of a picture

1. Draw the image with black pen on white paper.

1. Scan the image. Recommended settings. Picture might be an option but not tested
    1. High darkness
    1. Low resolution (e.g. 100x100 dpi)
    1. **Black & White**

1. Open the .pdf file / image with Inkscape.

1. Save it to .svg (not really necessary but makes easy to work with the files)
1. In Inkscape press *Path&gt;Trace bitmap*
    1. Settings:
        1. Brigthness cutoff (0,900) -&gt; PICTURE IS WRONG
1. You will get the vector lines. You can edit the lines if needed.
1. Go to Object&gt;Fill and Stroke. Set the fill to none  , strike black and 1pt width.
1. Go to File &gt; Document Properties and press Resize page to drawing or selection
1. Now it is time to setup the vinyl cutter for printing (see Vinyl cutting for more detailed instructions)
1. Turn on the vynil cutter
1. Select Edge
1. Using the arrows move the razor to your desired origo (bottom left corner of your design)
1. Press *Origin* button
1. In Inkscape Press the File &gt; Print button
1. Select Roland GS-24
1. Press Preferences
1. Adjust the printing settings
    1. In *Cutting Area*press *Get from machine*
    1. The length should be the height of your document (aprox.)
    1. Remove the rotation (Rotate *off)*
1. Press Print
