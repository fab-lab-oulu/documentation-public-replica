# About Super Fab Lab Oulu

Super Fab Lab Oulu is a unit operating within University of Oulu. It is a
small scale workshop for digital fabrication (FABrication LABoratory)
that provides facilities and training for digital fabrication.

Super Fab Lab Oulu comprises off-the-shelf, industrial-grade fabrication and
electronics tools, using open source software, as well as in-house made
equipment and software.

We are a Super Fab Lab, because in addition to all tools all Fab Labs have we have the equipment to build our own machines.

Fab Lab Oulu has the following machines, among others:

- A laser cutter that makes 2D and 3D structures,
- three 3D printers of different sizes and performance: a professional scale Stratasys machine, a resin-based machine for creating small scale and precision 3D parts, a Leapfrog 3D printer for rapid prototyping,
- A vinyl cutter for creating versatile 2D models on different materials such as vinyl, paper, copper sheets to make signs, indicators, T-shirt imprints, antennas and flex circuits,
- A high-resolution CNC milling machine that makes circuit boards and precision parts,
- A large wood router for building furniture and housing,
- A suite of electronic components and programming tools for low-cost, high-speed microcontrollers for on-site rapid circuit prototyping
- A space for meetings, training, and presentations comprising of 6 computers for 2D and 3D design, a large screen for presentations, and a white board.

Read more about [the Fab Lab concept](https://en.wikipedia.org/wiki/Fab_lab).

Visit the webpage of
Fab Lab Oulu [http://www.oulu.fi/fablab/](http://www.oulu.fi/fablab/)

<img src="attachments/68813709/69206402.jpg" width="900" />

## Fab Lab Oulu address

Fab Lab Oulu is located in the campus of the University of Oulu, rooms
TF 132-135, entrance 2T.

Finding your way inside the campus: walk along the "orange lane", Koneenkatu. Our premises are clearly marked as "Fab Lab Oulu", TF 135.
See the [campus map](http://www.oulu.fi/sites/default/files/content/files/Kartta_Linnanmaa_Map_A3_14_updatefor2017_1.pdf).

Street address: Erkki Koiso-Kanttilan katu 3.

The bus stop "Yliopisto E/P" is very near to its entrance, as well as a taxi stop.

See the [campus map](http://www.oulu.fi/sites/default/files/content/files/Kartta_Linnanmaa_Map_A3_14_updatefor2017_1.pdf)
for instructions on how to reach the Lab.

## Documentation contributors

This documentation would not have been possible without the contribution of different collaborators:

### Main contributors

- Dorina Rajanen
- Behnaz Norouzi
- [Iván Sánchez Milara](@ivanmilara)
- [Malte Wilhemm](@malte.wilhelm93)

### Other contributors

- Mikko Toivonen

## Attachments

- [Linnanmaa Campus map](attachments/68813709/69206403.pdf)
