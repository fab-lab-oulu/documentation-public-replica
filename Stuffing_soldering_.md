# Stuffing (soldering)

Stuffing means **assembling the parts on the board**.

First, the components are selected and marked on a paper when found.

Stuffing can be done by manual soldering using the electronics workbench
or by a more automated process using pick-and-place and reflow oven.

This tutorial explains both processes, but mainly the pick-and-place and
reflow oven usage.

An useful introduction into soldering is this article in Wikipedia:
[https://en.wikipedia.org/wiki/Soldering](https://en.wikipedia.org/wiki/Soldering)

## The Fab Lab Oulu electronics workbench

**<img src="http://www.oulu.fi/sites/default/files/styles/full_width/public/content-images/LR-34.jpg?itok=kca2vEAW" class="img-responsive" alt="Contacts" />**

## Stuffing using pick-and-place and the reflow oven

Before using this process, make sure you do not have FR1, but FR4 (FR1
does not resist the heat in the oven). Also make sure the components can
resist the oven.

The needed components are identified and marked. The schematic and board
layout are needed also to inform of where each component is placed and
what is the correct orientation.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/ButtonLed_stuffing.jpg)

We can do the **soldering** using a pick-and-place machine (Essemtec -
expert line) and a special oven to bake the board (LPKF Protoflow)
available at FabLab Oulu. This system is called surface mount technology
(SMT) for placing components with high precision and efficiency (aka
pick-and-place) and for soldering them on the board.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/pick_and_place.jpg)
![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/pick_and_place_2.jpg)

The process consists of:

1. Apply soldering paste to pads.

- Press the paste dispenser button on the system interface (first button on the left on the top row).
- The paste dispenser needle is set to be lower than the vacuum needle (by dragging it down by hand).
- The dispenser is moved manually to the correct position on the pad.
- When the needle touches the pad, use pedal (once) to apply the paste on the pad (this corresponds to a timer set to 0.7 s). The operation can be done without using the pedal by setting the timer correctly, e.g., 0.7 s for pads designated for resistors and alike, and 0.5 s for microcontrollers pads. When the needle touches the pad will automatically put the paste on it. If you use pedal, after releasing the pedal, let the needle on the pad a bit more until all paste is put on the pad and the tip of the needle remains clean.

1. Place the components on the pads using the suction system and needle (or nozzle).

- Set the position of the paste dispenser back to its place (higher than the suction needle).
- Press the placement button on the pick-an-place station (first button on the left on the bottom row) .
- Then place the components one at a time using the pump on the right side to control the suction and the direction of movement. When the component is on the correct place on the board release the suction pump.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/assembly1.jpg)

3\. Not all components can be placed using this system. E.g., the FTDI
connector was left aside to be soldered by hand.

4\. When placement is over, the pick-and-place machine can be shut down.

5\. Bake the board in the special oven. For operating the machine use
the up-down arrows to select the options that are shown on the small
screen and use the right arrow to press ENTER. The small blue screen
shows options, status, and instructions.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/LPKF.jpg)

- Open the oven.
- Set the distance between the rails by placing the board on the rails in a loose way so that the board can be taken away easily when hot.
- Take away the board from the rails.
- Close the oven.
- Select the profile (Profile settings) as LF-Medium.
- Start profile for warming up the oven up to 160 C. When this is ready Press enter to insert the PCB.
- Place the PCB with care because the rails are hot; grab the middle of the board.
- Close the oven drawer (ENTER) and the system will preheat again for 2 min up to 160 C. Then for about 100 s it will reflow to 235 C.
- When reflow is ready, the oven opens automatically and cools down for 80 s.
- Then it is safe to remove the PCB and press enter to close the drawer.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/assembly2.jpg)

Next, solder manually the components were not suitable with the pick-an-place and reflow oven.

## Manual soldering

Example, soldering the FTDI connector.

Use the electronics  workbench. Make sure you have the ventilation on.

Wash hands after finishing soldering.

Clean the workbench after completing your work.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/hand_soldering.jpg)

The final echo hello-world board assembly and the connection with the ISP programmer board are presented below.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/ButtonLed_assembly.jpg)
![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/ButtonLed_ISP.jpg)

Some further resources regaridng soldering:

- [Antti](http://archive.fabacademy.org/archives/2016/fablaboulu/students/161/)
- [Juha](http://archive.fabacademy.org/archives/2016/fablaboulu/students/189/)
- [Dorina](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/)
- [Jani](index)
- [http://archive.fabacademy.org/archives/2016/doc/electronics_production_FabISP.html](http://archive.fabacademy.org/archives/2016/doc/electronics_production_FabISP.html)
- David's [documentation](http://fab.cba.mit.edu/content/projects/fabisp/)
- [Video lecture](https://vimeo.com/155713990) on electronics production.
