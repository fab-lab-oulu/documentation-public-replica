# :knife: Vinyl Cutting

> Vinyl cutters are used for producing labels, logos, and other freeform shapes. A computer-controlled blade cuts the material (vinyl or thin cardboard) into the desired shape. The material can be self-adhesive, making it very easy to create stickers.

## :question: How to use the vinyl cutter

| :warning: tl;dr          |
|--------------------------|
| Prepare file properly and check blade cutting depth. |

| Aspect                   | Details                              |
|--------------------------|--------------------------------------|
| Fill                     | None                                 |
| Stroke paint             | any color (e.g., black [0 0 0 255])  |
| Stroke style             | Dashes: continuous                   |
| Width                    | 0.500 pt                             |

### Step 1: File Preparation

* Your vinyl cutter files must contain only vectors.
* The vectors must be lines only, with no fill.
* The vinyl cutter drivers can only interpret cut lines.
* All vectors must be contained within the bounds of the document canvas.

If you open a new file in inkscape, on the computer next to the viny cutter, the default document dimensions should be correct (Width x Height: 566x16000 mm). The [default file](https://librearts.org/2011/02/creating-templates-for-inkscape/) is located at `%APPDATA%\Inkscape\templates\default.svg`. However, doublecheck if the dimensions are correct from "File" menu > "Document Properties". Vinyl is on a roll - so the maximum height could be as long as the length of vinyl on the roll.

For optimal use of vinyl material when printing, the model should be placed *in the lower-left corner of the page*. Used the alignment and distrubute options to help you with that. Select **Relative to page** and move the objects to the bottom and the left of the page.

![Inkscape Align](images/inkscape-align.png)

### Step 2: Fill and Stroke

Select all Vectors. Then click "Object" menu > Fill and Stroke. The fill and stroke panel will appear to on the right of the screen. Make sure there is no fill by clicking on the "X" button in the "Fill" menu. Click the stroke paint tab. Make sure "R" is at the max - "255". Go to stroke style and make sure width is set to 0.500 pt.

![Inkscape Fill and Stroke](images/inkscape-fill-stroke.png)

### Step 3: Prepare the machine

* Turn on the machine
* Insert the vinyl sheet and align it to the edge when the lever is down; put the lever up when the vinyl is aligned.
* On the console use up and down arrows to select EDGE and press Enter button, if you want the model to be printed at the edge of the vinyl sheet. Other options allow you to print the model in other areas of the sheet.
* Set origin
    + use arrows to adjust cut origin
    + press the origin button until Origin Set is shown

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week3/vinyl_setting.png)

Move the pinch rollers to the edges of the material so that each one is within the grit marks.

![alt text](images/cutter-bounds.jpg)

### Step 4: Cut your design

Select Extensions > Roland CutStudio > Open in CutSudio. A new window will open. Press Cutting > OK.

![Inkscape Roland CutStudio](images/inkscape-roland-cutstudio.png)
![Roland CutStudio Cutting](images/roland-cutstudio-cutting.png)

### Step 5: Weed the Vinyl

*Weeding* the cut vinyl is the process of removing the unwanted areas from the final cut design.
After cutting, trim area you want to keep with scissors. Then remove the unwanted parts of vinyl with an X-Axcto knife, box knife and / or tweezers.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week3/vinyl_step3.jpg)
![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week3/vinyl_step5.jpg)

### Step 6: Transfer to the Desired Surface

Carefully apply transfer tape to the surface of the weeded vinyl. Rub the transfer tape onto the vinyl to form a strong bond between the tape and the top surface of the vinyl design.

After rubbing, carefully pull the transfer tape (with the vinyl adhering to it) away from the vinyl paper backing.

After you have removed the vinyl from the paper backing and it is attached to the transfer tape, apply it to the desired surface. Rub the surface of the transfer tape to ensure that the vinyl will adhere to the new surface.

Carefully pull the transfer tape away from the vinyl, leaving the vinyl attached to the desired surface.

![](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week3/vinyl_step6.jpg)

## Advanced Usage

### Cut smaller pieces

If you want to cut pieces that are smaller than the widht of a roll, you need to trick the sensor on the viny cutter. Simplfy tape the samller piece to a piece of paper.

![Cut smaller pieces by tapping them on paper](images/cutter-pieces.jpg)

## Common errors

### BAD POSITION

#### Cause

​The pinch roller(s) are not properly located to the grit roller. The is the area where there is more traction on the roller. Move sure material covers the sensor.

#### Solution

​Press any key to cancel setup and clear the error message. Move the pinch roller assemblies to the correct positions and reload the material.

## Additional resources

* [User Manual](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_index.html)
