# Fab Lab Oulu Wiki Space

Welcome to our wiki space!

Icon

This is the home page for the documentation of Fab Lab Oulu within
Confluence.

**Next you might want to:**

- **Learn about [Fab Lab Oulu](About_Fab_Lab_Oulu)**
- **Learn about Fab Lab Oulu safety rules, machines, processes and practices** - by selecting the corresponding item from left-side panel
- **Book a time for a Fab Lab Oulu machine** with our Scheduler system  - [http://fablab.virtues.fi/fablab/](http://fablab.virtues.fi/fablab/)
- **Visit our main Fab Lab Oulu page** where there is information on activities, projects, and training - [http://www.oulu.fi/fablab/](http://www.oulu.fi/fablab/)
- Visit us at the Fab Lab. Here is the [campus map](http://www.oulu.fi/sites/default/files/content/files/Kartta_Linnanmaa_Map_A3_14_updatefor2017_1.pdf) and [contact information](http://www.oulu.fi/fablab/node/32345).

## Search this documentation

Search
