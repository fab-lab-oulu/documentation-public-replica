# Laser cutter

## **Epilog Laser Fusion **-**** laser cutter for 2D/3D design and fabrication

### Model: **Epilog Fusion M2 40 Laser**

With an oversized table of 40" x 28" (**1016 x 711 mm**), the Fusion M2 40 features Epilog's largest work area.

This machine was designed to engrave the same high-quality image at any
point on the table by utilizing Epilog's top-of-the-line motion control
system and industry-leading optics system.

<img src="http://www.oulu.fi/sites/default/files/fablab_laser.jpg" width="742" height="417" />

**Vendor website:** [https://www.epiloglaser.com/](https://www.epiloglaser.com/)

### Technical Specifications

|                      | Epilog Fusion M2 40 |
|----------------------|---------------------|
| Engraving Area       | 40" x 28" (1016 x 711 mm) |
| Maximum Material Thickness | 13.25" (336 mm) |
| Laser Wattage        | CO2: 75 watts       |
| Laser Source         | State-of-the-art, digitally controlled, air-cooled CO2 laser tubes are fully modular, permanently aligned and field replaceable. |
| Intelligent Memory Capacity | Multiple file storage up to 128 MB. |
| Air Assist           | Attach an air compressor to our included Air Assist to remove heat and combustible gases from the cutting surface by directing a constant stream of compressed air across the cutting surface. |
| Laser Dashboard      | The Laser Dashboard™ controls your Epilog Laser's settings from a wide range of software packages - from design programs to spreadsheet applications to CAD drawing packages. |
| Red Dot Pointer      | Since the laser beam is invisible, the Red Dot Pointer on the Fusion Laser allows you to have a visual reference for locating where the laser will fire. |
| Relocatable Home     | When engraving items that are not easily placed at the top corner of the laser, you can set a new home position by hand with the convenient Movable Home Position feature on the Fusion Series Lasers. |
| Operating Modes      | Optimized raster, vector or combined modes. |
| Motion Control System | High-speed Brushless DC Servo Motors. |
| X-Axis Bearings      | Ground and polished stainless steel Long-Lasting Bearing System. |
| Belts                | Kevlar (x-axis) and Steel Cord (y-axis) belts. |
| Resolution           | User controlled from 75 to 1200 dpi. |
| Speed and Power Control | Computer or manually control speed and power in 1% increments to 100%. Vector color mapping links speed, power and focus to any RGB color. |
| Print Interface      | 10 Base-T Ethernet or USB Connection. Compatible with Windows® XP/Vista/7/8/10. |
| Size (W x D x H)     | 60.5" x 41.25" x 42.25" (1537 x 1048 x 1073 mm) 43" (1092 mm) deep with exhaust plenum. |
| Weight               | 643 lbs (292 kg)    |
| Electrical Requirements | Auto-switching power supply accommodates 110 to 240 volts, 50 or 60 Hz, single phase, 15 amp AC. |
| Maximum Table Weight | Fusion 32/40: 200 lbs (90 kg) for static and 100 lbs (46 kg) for lifting. |
| Ventilation System   | 650 CFM (1104 m³/hr) external exhaust to the outside or internal filtration system is required. There are two output ports, 4" (102 mm) in diameter. |

See guidelines on [Laser cutting](Laser_cutting).

See [safety rules of using the laser cutter and other equipment](Safety_rules_in_Fab_Lab_Oulu).

See [Materials allowed and not allowed with the laser cutter](Materials_allowed_not_allowed_with_the_laser_cutter).
