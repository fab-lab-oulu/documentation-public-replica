# Laser cutting

See [Safety rules of using the laser cutter and other
equipment](Safety_rules_in_Fab_Lab_Oulu).

See [Materials allowed and not allowed with the laser
cutter](Materials_allowed_not_allowed_with_the_laser_cutter).

See [Information about the laser cutter](Laser_cutter).

See [2D design tutorials](Computer-aided_2D_design) (e.g.,
[Inkscape](Inkscape), [Gimp](GIMP)).

Using the laser cutter is easy; there are some [safety rules](Safety_rules_in_Fab_Lab_Oulu) and operation instructions that you have to follow.

Not any material can be used with the laser cutter; please see
[Materials allowed and not allowed with the laser cutter](Materials_allowed_not_allowed_with_the_laser_cutter). Never use
unknown materials with the laser cutter!

The file should be prepared in .pdf format. The design can be done in
any graphical and image processing program such as Inkscape, Photoshop,
etc. and then saved or exported as a .pdf file.

## Tutorial videos

This video show how to operate the laser cutter. For more detail information follow the tutorial below:

- [Laser leikkuri tutoriaali](attachments/69797308/79593484.mp4)
    (Suomeksi)
- [Laser cutter tutorial (English)](attachments/69797308/79593488.mp4)

## Preparing the file

The requirements for cutting and engraving are as follows with regard to
**Fill and Stroke settings** (e.g., in Inkscape):

**Settings for laser cutting (vector):**

- No Fill
- Stroke paint = black or RGB \[0 0 0\]
- Stroke style = 0.02 mm and continuous line

**Settings for laser engraving (raster):**

- Fill = black (other colors are also possible\*)
- Stroke paint = No stroke (otherwise will also cut the shapes)

When using other color(s) than black, check and adjust the settings
of the laser cutter parameters according to your needs using the laser
dashboard (see Figure 1 below). You can use different colors with
different parameters for engraving for creating different engraving
effects such as depth.

<img src="attachments/69797308/70648063.png" width="750" />

*Figure 1: Color mapping tab in the laser dashboard.*

## Saving the model for laser cutter

Save your model in the **.pdf** format.

A .pdf file can also be open in Inkscape and edited if necessary, for
example to comply with the requirements for line thickness (0.02 mm) for
cutting.

## Safety rules

- First, when starting a job with the laser cutter, ensure that the air input and output are open (see instruction on the wall near the machine).

- The air input or air assist should always be activated during laser cutting and engraving to reduce the risk of fire.

- The air output or exhaust should always be activated during laser cutting and engraving to remove the dust, debris, and the smell and toxic gases.

<img src="attachments/69797308/70648061.jpg" width="900" />

- If you have a material that you are not sure it is suitable for laser cutter, consult the list of allowed /not allowed materials or ask the Fab Lab Oulu staff for guidance.
- **Do NOT cut unknown materials!**

- Never leave the machine unattended.
- At the end of the job, wait one minute before you open the lid. In this way you allow the toxic gases to be removed from the machine and not enter the room.
- Wash your hands with water and soap after finishing your work.

## Operation instructions

**Ensure that the air input and output are open (see instruction on the
wall near the machine).**

<img src="attachments/69797308/70648061.jpg" width="900" />
**If the machine is shut down, turn on the laser cutter machine.**
<img src="attachments/69797308/70648040.png" width="900" />

Open the .pdf file and send it to the printer (Epilog laser engrave).
File -&gt; Print -&gt; Properties (Ominaisuudet) -&gt; chose laser
cutting parameters -&gt; Print.

![](attachments/69797308/70648042.png)

The setting of the laser cutter parameters depends on the material used
and the depth of cutting and engraving you aim for.
There are predefined values for most typically used materials such as
MDF, plywood, acrylic.

To set the laser parameter you have to use the **Laser Dashboard** (see
picture below; the laser dashboard is a print driver that has to be
installed on the computer connected to the laser machine; the print
driver send the laser parameters and the design from computer to the
laser).
The predefined values of the cutting and engraving parameters can be
loaded from Advanced tab (as shown in the picture below that illustrates
the selection of 4mm thick acrylic).

![](attachments/69797308/70648043.png)
*Figure 2. Advanced tab for selecting predefined values for a certain
material*

At General Tab you can further adjust the parameters if needed. See
below. When parameters are correctly set, press OK.
![](attachments/69797308/70648044.png)

Typically the following selections are done:

1. Job Type:
    1. Raster: when you only want to engrave.
    1. Vector: when you only want to cut.
    1. Combined: when you want to both cut and engrave.
1. Options: Check Center-engraving if the origin or the reference point
    (see below at step 5) is set in the center of the image to be engraved/cut. By default, the reference point is the upper left corner of the image.
1. Raster settings for speed, power, and frequency.
1. Vector settings for speed, power, and frequency.

You can find more information on the settings in the [Epilog laser
manual](https://www.epiloglaser.com/assets/downloads/manuals/fusion-manual-web.pdf),
for example about the color mapping (see picture below).

<img src="attachments/69797308/70648062.png" width="750" />

Place the material on the table in the machine and set the focus.
Setting the focus means setting the distance from the bottom of the
focus lens to the top of the material; it is done by placing the
material on the table and moving the table up or down using the control
panel (see figure below).
Typically, the material is placed on the upper-left corner of the
table.

Procedure for setting the focus manually:

1. Place the material to be cut or engraved in the upper left corner of the table.
1. Place the manual focus gauge on the lens carriage.
    ![](attachments/69797308/70648068.png)
    *Figure 3. Focus gauge and placing the focus gauge on the lens carriage.*

1. Select **Focus** on the laser machine's control panel using the
    **arrow keys**.
1. Use the **joystick** to move up or down the table until the material just touches the bottom of the gauge.
1. When the focus has been achieved, press the joystick to **set the focus to 0.00** as in the figure below.
1. Remove the gauge.
    <img src="http://archive.fabacademy.org/archives/2017/fablaboulu/students/70/images/week3/Laserinohjaus.jpg" width="500" />
    *Figure 3. Setting the focus to 0.00. (Photo source: http://archive.fabacademy.org/archives/2017/fablaboulu/students/70/week\_3.html)*

    <img src="attachments/69797308/70648067.png" width="300" />
    *Figure 5. Setting the focus (photo source:*
    *[http://archive.fabacademy.org/archives/2017/fablaboulu/students/70/week\_3.html](http://archive.fabacademy.org/archives/2017/fablaboulu/students/70/week_3.html))*

**Set the origin or the reference point**. This is the point on the
material that corresponds to the upper left corner of the document
containing the artwork to be cut or engraved. (When center-engraving,
the origin should be set in the center of the material's surface to be
engraved.) Procedure for setting the origin:

1. Place the material to be cut or engraved in the upper left corner of the table (or anywhere on the table).
1. Select **JOG** on the laser machine's control panel using the
    **arrow keys**.
    <img src="attachments/69797308/70648066.png" width="500" />
    *Figure 6. Selecting JOG on the control panel. (Photo source:
    [http://archive.fabacademy.org/archives/2017/fablaboulu/students/77/week3.html](http://archive.fabacademy.org/archives/2017/fablaboulu/students/77/week3.html))*

1. Use the **joystick** to move the lens carriage to the desired position.
1. When the origin was chosen, press the joystick to **set the origin
    (it will show on the control panel's screen +0.02 +0.02**).

**Press Go to perform the cutting/engraving job.**

1. Use the arrows on the control panel to select **JOB.**
1. On the control panel's screen is displayed the name of the file sent to the laser. Check that the name of the job corresponds to the file you sent.
1. The duration of the job is also displayed.
1. Press **GO** on the control panel if everything is ok. **The job will start**.

**Finishing and cleaning up the work space.**

1. At the end of the job, **wait 1 min** before you open the lid. In this way you allow the gases to be removed.
1. Remove the material and clean the work space you have used.
1. Close your files. Delete your files from desktop or store them in the "C:\\Lasercut archive" folder.

More information about the laser cutter in the online Epilog laser manual. In Fab Lab there is also a printed manual you can consult or ask staff for help.
