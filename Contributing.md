# Contributing to Fab Lab Oulu Wiki

## Why We Use A Replica Repository

It's essentially a workaround, also used by the [Tor Project](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#accepting-merge-requests-on-wikis), to enable merge request on wikis.

### Key Reasons for Using the Merge Requests

- **Controlled Contributions**: Changes are reviewed before being integrated, maintaining the quality and consistency of the wiki.
- **Broader Participation**: More users can suggest changes without having direct write access to the main wiki.
- **Version Control**: GitLab’s version control features help manage contributions and track changes effectively.

## Who to Ask for Permissions

If you need permissions or have any questions regarding contributions, you should contact:

**Iván Sánchez Milara**
University Teacher
Fab Lab Oulu
Email: [ivan.sanchez@oulu.fi](mailto:ivan.sanchez@oulu.fi)

## Contribute using GitPod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/fab-lab-oulu/documentation-public-replica)

## Steps to Contribute Using GitLab WebIDE

1. **Access the Repository**
    + Navigate to the [Fab Lab Oulu Documentation Public Replica](https://gitlab.com/fab-lab-oulu/documentation-public-replica) repository on GitLab.
    + Ensure you are logged in to your GitLab account.

1. **Open WebIDE**
    + On the repository’s main page, click the `Edit` button and select `WebIDE`.

   ![WebIDE](/images/WebIDE.png)

1. **Make Changes**
    + Use the file explorer on the left to navigate to the file you want to edit or create a new file.
    + Click on the file to open it in the editor.
    + Make the necessary changes or additions to the documentation.

1. **Commit Changes**
    + Once you have made your changes, click on the `Source Control` icon in left panel.
    + Enter a commit message describing your changes.
    + Ensure the `Commit to a new branch` option is selected.
    + Click on `Commit`.

    ![Commit](/images/commit.png)

1. **Push the Branch**
    + After committing, click on the `Push` button in the top-right corner of the WebIDE to push your new branch to the remote repository.

1. **Create a Merge Request**
    + After pushing your branch, navigate back to the repository’s main page.
    + Go to `Code --> Merge Requests`.
    + Click on `New Merge Request`.
    + Select your new branch as the source branch and `master` as the target branch.
    + Provide a descriptive title and detailed description for your merge request.
    + Click on `Submit Merge Request`.

1. **Review and Merge**
    + The repository maintainers will review your merge request, check the accuracy, relevance, and quality of the changes.
    + If necessary, they may request additional modifications or clarifications.
    + Once approved, the changes will be merged into the `master` branch of the replica repository and then synchronized with the main Fab Lab Oulu Wiki.
