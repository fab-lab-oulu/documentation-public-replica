# Inkscape

Inkscape is a vector graphics program which is open source and very
useful for creating all kinds of 2D graphics similar to the ones created
by professionals in Adobe Illustrator and CorelDraw.

To install Inkscape: [https://inkscape.org/en/](https://inkscape.org/en/)

## Important notes

**When creating 2D models for being processed with the laser cutter, the following rules regarding Fill and Stroke apply:**

Settings for laser cutting (vector):

- No Fill
- Stroke paint = black or RGB \[0 0 0\]
- Stroke style = 0.02 mm and continuous line

Settings for laser engraving (raster):

- Fill = black (other colors are also possible\*)
- Stroke paint = No stroke (otherwise will also cut the shapes)

When using other color(s) than black, check and adjust the settings
of the laser cutter parameters according to your needs. You can use
different colors with different parameters for engraving for creating
different engraving effects such as depth.

To change the order laser cuts and engraves, you need to change object order. The laser cuts the object in inverse order starting from bottom of the list. It may be useful to engrave before cutting to prevent the piece from moving.

## Saving the model for laser cutter

Save your model in the **.pdf** format.
