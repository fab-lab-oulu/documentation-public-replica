# Safety issues with the Form 2 printer

## Safety issues (based on [vendor safety information](https://support.formlabs.com/hc/en-us/articles/115000011604-Safety))

The Form 2 is a precision tool that requires respect and care to ensure
safe operation. Follow all safety considerations during operation.

Like any professional equipment, you should treat the printer,
materials, and accessories with respect and care to ensure a safe
working environment and a long-lasting machine.

### Supervision

The Form 2 is an excellent educational tool.

The tool should be used with proper training and the supervision of
young, inexperienced users to ensure enjoyable and safe operation.

### Do Not Modify

The Form 2 printer is for use as-is. The printer should not be modified
without explicit approval and directions from Formlabs, otherwise
warranty will be invalid, the machine can be ruined, and can cause
bodily harm to users.

### Laser

![](https://support.formlabs.com/hc/en-us/article_attachments/115000025450/lasercaution.png)Caution
when working with lasers.

The Form 2 is a Class 1 Laser product. Accessible radiation is within
Class 1 limits. The laser diode used inside the device has the following
specifications:

**Diode**: Violet (405nm)

**Max Output**: 250 mW

Never remove the front or back panels of the printer unless instructed
to do so by Formlabs Support. This will expose you to danger and void
your
[warranty.](https://formlabs.com/support/terms-of-service/#warranty) The
laser beam is harmful to the eyes, so avoid direct contact. The Form
2 contains an interlock system to automatically shut off the laser when
the orange cover is opened. If this system is tampered with, or fails,
there is risk of exposure to Class 3B laser light.

#### Laser Certification

– IEC 60825-1:2007
– EN 60825-1:2007
FDA performance standards for laser products except for deviations
pursuant to Laser Notice No. 50, dated June 24, 2007.

#### WARNING 1

**Class 1 Laser Product. Never remove the front or back panels of the printer.**

### Resin

Respect Formlabs resin like any household chemical. Follow standard
chemical safety procedures and [Formlabs resin handling
instructions](https://support.formlabs.com/hc/articles/115000018044).
Wear gloves whenever handling liquid resin.

In general, Formlabs resin is not approved for use with food, drink, or
medical applications on the human body. However, biocompatible resins,
such as Dental SG, are biologically safe for specific types and lengths
of exposure to the human body. Refer to information about each specific
resin for more detail. Never ingest resin in liquid or solid form.

#### TIP

Always consult the [SDS (Safety Data
Sheet)](http://formlabs.com/products/3d-printers/tech-specs/#material-properties)
as the primary source of information to understand safety and handling
of Formlabs materials.

### Isopropyl Alcohol (IPA)

![](https://support.formlabs.com/hc/en-us/article_attachments/115000024444/flammablehazardsymbol.png)Isopropyl
alcohol is a flammable chemical.

Carefully follow the safety instructions provided with the isopropyl
alcohol that you purchase. Isopropyl alcohol can be flammable, even
explosive, and should be kept away from heat, fire, or sparks. Any
containers holding isopropyl alcohol should be kept closed or covered
when not in use. We also recommend that you wear protective gloves and
have good ventilation when working with IPA.

#### WARNING 2

Formlabs does not manufacture isopropyl alcohol. You should consult the
chemical manufacturer or supplier for more in depth safety information.
[Learn how to handle IPA with your
Form 2](https://support.formlabs.com/hc/articles/115000024624).

### Sharp Tools

The accessories kit includes sharp tools such as: tweezers, ﬂush
cutters, a scraper, and a part removal tool. Using these tools on
slippery surfaces (such as a resin-coated build platform) can result in
sudden movement.

### Radio Interference

This equipment has been tested and found to comply with the limits for a
Class B digital device, pursuant to CFR Title 47, Part 15 of FCC Rules.
These limits are designed to provide reasonable protection against
harmful interference when the equipment is operated in a commercial
environment. This equipment generates, uses, and can radiate radio
frequency energy and, if not installed and used in accordance with the
instruction manual, may cause harmful interference to radio
communications. Operation of this equipment in a residential area is
likely to cause harmful interference in which case the user will be
required to correct the interference at their own expense.
