# Computer-aided 2D design

When using the laser cutter or the vinyl cutter, you need to have the
design files for your models ready.

You can do the design using any 2D graphics tools such as Adobe, GIMP,
Inkscape.

Fab Lab Oulu has a computer space room where you can do your design
using the installed open source software GIMP (for raster graphics) and
Inkscape (for vector graphics).

See guidelines on using [Inkscape](Inkscape).

See guidelines on using [GIMP](GIMP).
