# Precision CNC milling

## **Roland milling **-**** A high precision milling machine for making circuits and molds for casting

### Model: Monofab SRM-20

As a small milling machine, the SRM-20 offers compact size and powerful
functionality.

Production of realistic parts and prototypes is made simple and
convenient with a device that fits into any office, home, or classroom
environment.

For users looking for advanced milling capabilities without the need for
expert operating skills, the SRM-20 is the easiest and most precise CNC
mill in its class.

<img src="https://www.oulu.fi/sites/default/files/fablab_cutter.jpg" width="760" height="427" />

**Vendor website**: [https://www.rolanddga.com/](https://www.rolanddga.com/)

### Technical specifications

<table class="confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">MODEL</th>
<th class="confluenceTh">SRM-20</th>
</tr>

<tr class="odd">
<td class="confluenceTd">Cuttable Material</td>
<td class="confluenceTd">Modeling Wax, Chemical Wood, Foam, Acrylic, Poly acetate, ABS, PC board</td>
</tr>
<tr class="even">
<td class="confluenceTd">X, Y, and Z Operation Strokes</td>
<td class="confluenceTd">8 (X) x 6 (Y) x 2.38 (Z) inches,<br />
203.2 (X) x 152.4 (Y) x 60.5 (Z) mm</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Workpiece table size</td>
<td class="confluenceTd">9.14 (X) x 6.17 (Y) inches,<br />
232.2 (X) x 156.6 (Y) mm</td>
</tr>
<tr class="even">
<td class="confluenceTd">Distance From Collet Tip to Table</td>
<td class="confluenceTd">Maximum, 5.15 in<br />
(130.75mm)</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Loadable Workpiece Weight</td>
<td class="confluenceTd">4.4 lbs (2kg)</td>
</tr>
<tr class="even">
<td class="confluenceTd">X-, Y-, and Z-Axis Drive System</td>
<td class="confluenceTd">Stepping motor</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Operating Speed</td>
<td class="confluenceTd">0.24 – 70.87inch/min,<br />
6 – 1800mm/min</td>
</tr>
<tr class="even">
<td class="confluenceTd">Software Resolution</td>
<td class="confluenceTd">0.000039 inches/step (RML-1), 0.000039 inches/step (NC code),<br />
0.01 mm/step (RML-1), 0.001mm/step (NC code)</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Mechanical Resolution</td>
<td class="confluenceTd">0.0000393 inches/step,<br />
0.000998594 mm/step</td>
</tr>
<tr class="even">
<td class="confluenceTd">Spindle Motor</td>
<td class="confluenceTd">DC motor Type 380</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Spindle Rotation Speed</td>
<td class="confluenceTd">Adjustable 3,000 – 7,000 rpm</td>
</tr>
<tr class="even">
<td class="confluenceTd">Cutting Tool Chuck</td>
<td class="confluenceTd">Collet method</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Interface</td>
<td class="confluenceTd">USB</td>
</tr>
<tr class="even">
<td class="confluenceTd">Control Command Sets</td>
<td class="confluenceTd">RML-1, NC code</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Power Requirements</td>
<td class="confluenceTd">Machine: DC24V, 2.5A,<br />
Dedicated AC adapter: AC 100-240V ±10%, 50/60Hz</td>
</tr>
<tr class="even">
<td class="confluenceTd">Power Consumption</td>
<td class="confluenceTd">Approx. 50W</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Operating Noise</td>
<td class="confluenceTd">During operation: 65 dB (A) or less (when not cutting),<br />
during standby: 45 dB (A) or less</td>
</tr>
<tr class="even">
<td class="confluenceTd">External Dimensions</td>
<td class="confluenceTd">17.76 (W) x 16.80 (D) x 16.78 (H) inches,<br />
451.0 (W) x 426.6 (D) x 426.2 (H) mm</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Weight</td>
<td class="confluenceTd">43.2 lbs, 19.6 kg</td>
</tr>
<tr class="even">
<td class="confluenceTd">Installation Environment</td>
<td class="confluenceTd">Temperature of 41 to 104 °F (5 to 40°C),<br />
35 to 80% relative humidity (no condensation)</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Included Items</td>
<td class="confluenceTd">USB cable, AC adapter, Power cord, Cutting tool, Collet, Set<br />
screw, Spanners (7,10mm / 0.28, 0.39 inches), Hexagonal<br />
wrench (size 2,3 mm / 0.059, 0.12 inches), Positioning pins,<br />
Double-sided tape, Start-up page guidance card</td>
</tr>
</tbody>
</table>

See guidelines on [Milling a PCB](Milling).
