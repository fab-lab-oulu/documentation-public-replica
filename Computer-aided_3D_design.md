# Computer-aided 3D design

There are several programs for 3D design, some are commercial and some
are open source.

In Fab Lab Oulu, we have two commercial SW for 3D design installed on
the 6 computers in the computer space:

- Solid Works,
- Autodesk Inventory.

As free, open source software we recommend the following SW, but you can
choose any other tool you like to work with:

- FreeCAD,
- Autodesk 123D Design.

Some tutorials:

- [Autodesk 360 Fusion Part 1 (For absolute beginners)](Computer-aided_3D_design). See also other tutorials from the same instructor ([Lars Christensen](https://www.youtube.com/channel/UCo29kn3d9ziFUZGZ50VKvWA)) in YouTube.
