# Milling

This page describes the process of milling a PCB using the Roland monofab SRM-20.

**You can find a video explaining the process in the [following link](https://youtu.be/-7NV_vIO7Lk).**

**Input files**: image files (.png) with traces and cutout layouts for the PCB board.

Example of image files for traces and cutout:

<img src="https://lh5.googleusercontent.com/QHXXgXQuhHjHhVlHVY9S_aBugKLr64nxO3vMZFVylMir9nVUFpiat9gO6JNrIk5gYX78tjrWy_6i8vK3KxD9LExNXvBSMgSNhnpCwsTxvdnT6BZfbYH6yXNTvmy4THAEEokHfXqJ" width="300" />

Check point: both files should have the same size.

**Download example files**:
[traces.png](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/Traces_ButtonLed.png) and [cutout.png](http://archive.fabacademy.org/archives/2016/fablaboulu/students/352/assets/img/portfolio/week6/Cutout_ButtonLed.png)

**Machine**: Roland monofab SRM-20.

<a title="Creative Tools from Halmstad, Sweden, CC BY 2.0 &lt;https://creativecommons.org/licenses/by/2.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:3D_Printshow_2014_London_-_Roland_DG_monoFab_SRM-20_Desktop_CNC_milling_machine_(14964338568).jpg"><img width="512" alt="3D Printshow 2014 London - Roland DG monoFab SRM-20 Desktop CNC milling machine (14964338568)" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/3D_Printshow_2014_London_-_Roland_DG_monoFab_SRM-20_Desktop_CNC_milling_machine_%2814964338568%29.jpg/512px-3D_Printshow_2014_London_-_Roland_DG_monoFab_SRM-20_Desktop_CNC_milling_machine_%2814964338568%29.jpg?20180322002914"></a>

Check point: ensure you have the right miling bits and materials
available. Moreover, when designing the boards, keep in mind the
dimensions of the tools for setting the correct traces and clearance
parameters in Eagle (typically 16 mil for clearance should be ok).

1\) milling bit for traces:

- 1/64 inch (0.4) diameter (especially suitable for FR1 boards) or
- PCB Isolation milling V tool with diameter 0.2-0.5 mm especially suitable for FR4 boards.

2\) milling bit for cutout:

- 1/32 inch (0.79 mm) diameter or similar bith with up to 1.00 mm diameter.

3\) FR1 or FR4 boards. FR4 is suitable for being used in the reflow
oven; FR1 requires manual soldering, does not stand the heat in the
reflow oven.

**The main steps are:**

- create from the .png files the .rml files the the milling machine will use (these rml files are created with
    [fabmodules.org](http://fabmodules.org/))
- set up the milling machine and mill the board (use VPanel for SRM-20).

## Preparing the .rml files for milling

Use [fabmodules.org](http://fabmodules.org/) web application.

In fabmodules, the steps are:

1\) select the input format: image (.png)

2\) select the output format: Rolland mill (.rml)

3\) select process: PCB outline (1/32) or PCB traces (1/64)

4\) Adjust the milling parameters

5\) Press **calculate**

6\) Press **save.**

Three .rml files are created:

- cutout\_step1.rml to **test the tracing parameters.** A bit for tracing is used. This step does not produce a cut, but an outline on the board and the purpose is to see if the cut depth parameter for tracing (e.g., 0.1 mm) is good enough or has to be increased. If the result is not satisfactory, the step can be repeated with a different value of the cut depth. The input file is e.g.,
    "cutout.png".
- traces\_step2.rml **to create the traces**. The same sharp bit is used as in step1.The input file is e.g., "traces.png".
- cutout\_step3.rml **to cut the board**. A 1/32 bit is used.The input file is e.g., "cutout.png".

For each file above, different milling parameters are used as shown in
the pictures below.

### **Creating cutout\_step1.rml to test the tracing parameters.**

The input file is e.g., "cutout.png".

A bit for tracing is used. This step does not produce a cut, but an
outline on the board and the purpose is to see if the cut depth
parameter for tracing (e.g., 0.1 mm) is good enough or has to be
increased. If the result is not satisfactory, the step can be repeated
with a different value of the cut depth.

![](attachments/69796493/69894235.png)

Inspect the tool paths and if satisfactory save the file.

If not satisfactory, check and adjust the parameters (e.g., the stock
thickness, cut depth, or tool diameter).

![](attachments/69796493/69894237.png)

### **Creating traces\_step2.rml to create the traces.**

The input file is e.g., "traces.png".

Same steps as above, but at process use PCB traces (1/64) and adjust the
cutting parameters.

The same sharp bit is used as in step1.

![](attachments/69796493/69894236.png)

Cutting parameters for the traces:

- the cut depth is 0.1 or 0.15
- tool diameter is 0.4 (when 1/64 bit is used, e.g., when using FR1 boards), or 0.3 when a sharper but is used (recommended for FR4 boards).

### **Creating cutout\_step3.rml to create the traces.**

The input file is e.g., "cutout.png". A 1/32 bit is used.

Same steps as above, but at process use PCB outline (1/32) and adjust
the cutting parameters.

![](attachments/69796493/69894238.png)

Cutting parameters for the cutout:

- the cut depth is 0.5
- tool diameter is 0.79 (when 1/64 bit is used), or for stronger bits lately used in Fab Lab Oulu 0.9 or 1.0 mm.
- stock thickness is 1.7 for FR1 or 1.75 for FR4.

Summary of cutting parameters in different steps and with different materials:

|                                             |               |                 |                 |
|---------------------------------------------|---------------|-----------------|-----------------|
|                                             | step 1 (test) | step 2 (traces) | step 3 (cutout) |
| tool diameter, depending on the milling bit | 0.79 - 1.00   | 0.3 - 0.4       | 0.79 - 1.00     |
| stock thickness FR1                         | 0.1           | n.a             | 1.7             |
| stock thickness FR4                         | 0.1           | n.a.            | 1.75            |
| cut depth                                   | 0.1 - 0.15    | 0.1 - 0.15      | 0.5             |

## **Setting up the milling machine and materials:**

This part of tutorial is based on the documentation written by Jari
Pakarinen.

The setting of the machine involves the following:

1. Set up the blank pcb to the milling machine.

1. Set the correct drilling bits to the machine.

1. Define origo and drill bit depth.

1. Import the milling files (.rml from fabmodules)

1. Milling and removing the ready made pcb

## **1. Set up the blank PCB to the milling machine.**

The biggest issue is to set the blank pcb to the milling machine so that
it is even. First attach the pcb blank to the bottom support with
2-sided tape. Make sure that there isn't any air or small particles
between. Then attach the bottom support to the milling machine bottom
plate, if it is not attached already. Use 2-sided tape there as well.

 ![](attachments/69796493/69894241.png)
Next attach the whole package to the milling machine. There are 4
screws. Slide the bottom plane in place, press firmly towards back of
the machine and tighten the screws. Now the machine is almost ready.

 ![](attachments/69796493/69894242.png)

## 2. Set up the correct milling bits

Next the correct milling bit must be placed to the machine.

If there is a milling bit in the machine, remove it using allen key. One
must take care not to drop the bit. It will break quite easily.

Don't push the drill all the way in to the spindle so you have some free
movement later when defining the milling depth.

![](attachments/69796493/69894245.png)

## 3. Define origin and the depth of the bit

After adding the right milling bit open Vpanel for SRM-20 that is the
milling machine user interface software.

![](attachments/69796493/69894246.png)

From move X/Y buttons at the middle of the screen move the drilling head
to the origo i.e. left front corner of blank pcb. When happy with origo,
press the Set Origin Point X/Y button on the right. This will set the
origo. It is easier to set the origo when you move the milling bit
closer to the surface. Take care not to hit it thought.

Next move the z-axes, using z-button up/down, down close to pcb surface.
Be carefull not to hit the sufrace. Use "cursor step" (move speed in
picture there) options to control your movement. Stop the mill bit head
close to the surface. Use Allen key to loosen the mill bit and ease it
to the surface. This will be the depth setting. Tighten the Allen screw
again and accept the new depth by pressing the Set origin Point Z-button
at left top of the VPanel. This will set the new z-axis zero point. You
can see the zero set on the left top side on the VPanel (Coordinates).

![](attachments/69796493/69894247.png)

This will finnish the milling machine setup. Time to load the milling
files and run them.

## 4. Import the rml files

This happens in VPanel. Push the Cut-button and an file selection window opens. Clear all old milling files by pushing the Delete/Delete all -button. Then use Add-button and find the file you want to mill.
Pressing Output-button will start the selected milling so make sure that everything else is set and you have the right file selected.

![](attachments/69796493/69894248.png)

## 5. Milling and removing the ready made pcb

Finally milling machine is doing it job and milling.

![](http://archive.fabacademy.org/archives/2017/fablaboulu/students/70/images/week4/Milling_milling_text.jpg)

When the job is finished remove the dust and clean the board and surrounding area using the vacuum.

Take care not to breath in the dust.

When all steps are completed (traces and cutout), again clean the working area, remove the milling bit from the machine and place it in the support for milling bits, put everything in the cabinet and leave the space clean.

For removing the PCB, use a small screwdriver that is in the drawer.

Wash the board. Sandpaper the board, if needed.

Wash your hands.

Next step is [soldering](Stuffing_soldering_) (stuffing or assembling the components on the board).
